# Mascotas :dog: :cat2: :rabbit: :snake: :sheep: :horse: :rat: :turtle: :goat: :cow2: :frog: :hamster:

Aplicación en la que se publican y mascotas que se perdieron o que han sido encontradas, basado en la ubicación geográfica.

---

## LEVANTAR LA APLICACIÓN

instrucciones para levantar nuestro propio servidor tanto en modo desarrollo primero como en modo de producción

## Develop :dragon:

Debemos contar con las siguientes **variables de entorno** en el servidor si queremos su total funcionamiento:

    NODE_ENV

entorno en el que estamos trabajando, "develop", "production"

para hacer andar el servicio de mail se necesita alguno de los dos usuarios y contraseñas, _no impide el uso de la aplicación no poner éstos"_:

    SENDING_BLUE_USER
    SENDING_BLUE_PASS

    GMAIL_MASCOTAS_USER
    GMAIL_MASCOTAS_PASS

#### Tradicional

Para levantar el entorno de desarrollo debemos contar con:

- MongoDB (ver. 4.2.X) en nuestra maquina corriendo en el puerto por defecto (27017)
- NodeJS (ver. 10.X.X)

no olvidarnos de las variables de entorno para su total funcionamiento

Clonamos el repositorio.

    git clone direcciónDelRepositorio

accedemos a la carpeta, aquí se encuentran dos carpetas _"server"_ y _"client"_, vamos a correr ambos en consolas diferentes, arrancaremos por el **server**, entramos en la carpeta server y corremos lo siguiente:

    npm install && npm run dev

dejamos esta consola abierta y abrimos otra, ahora ejecutaremos el servidor de **cliente**, posicionados en la carpeta del cliente corremos el siguiente comando:

    npm install && npm run dev

se abrirá un navegador con la aplicación en modo de desarrollo.

---

#### Docker :whale2:

Para levantar el entorno de desarrollo con docker debemos contar con:

- NodeJS (ver. 10.X.X)
- Docker
- Docker-compose

También crear un archivo en ./docker/development/dev.env con los valores necesarios de las _variables de entorno_

Clonamos el repositorio.

    git clone direcciónDelRepositorio

Accedemos a las carpetas _"server"_ y _"client"_ y en cada una realizamos el siguiente comando

    npm install

o nos paramos en root del repositorio y hacemos

    cd ./server && npm install && cd ../client && npm install

luego vamos al directorio _"./docker/development"_ y ejecutamos

    docker-compose up

esto levantará el servidor el cliente y una base de datos que por defecto se encuentra vacía, cabe destacar que se posee "hot reload" tanto en el cliente como en el servidor.
