import React from "react"
import { Avatar } from "@material-ui/core"

function getInitials(name = "") {
  return name.map(v => v && v[0].toUpperCase()).join("")
}

export default function(props) {
  return (
    <Avatar {...props}>{getInitials([props.name, props.last_name])}</Avatar>
  )
}
