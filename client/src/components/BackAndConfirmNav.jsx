import React from "react"
import { Button, Grid } from "@material-ui/core"

export default function BackAndConfirmNav(props) {
  return (
    <Grid container align="center" justify="center">
      <Grid item xs={10}>
        <Grid container>
          {props.back ? (
            <Grid item xs={6} align="left">
              <Button color="primary" size="large" onClick={() => props.back()}>
                {props.backText || "Atrás"}
              </Button>
            </Grid>
          ) : null}
          <Grid item xs={props.back ? 6 : 12} align="right">
            <Button
              variant="contained"
              color="primary"
              size="large"
              disabled={props.disableAccept}
              onClick={() => props.accept()}
            >
              {props.acceptText || "Aceptar"}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}
