import React from "react"
import { Link } from "react-router-dom"
import { Fab } from "@material-ui/core"
import AddIcon from "@material-ui/icons/Add"
/**
 * Botón flotante con cualidad de reactRouter Link
 * @param {"pathname" a donde y "from" de la dirección actual} fBProps
 */
export default function AddFloatButton(fBProps) {
  return (
    <Fab
      style={{
        position: "fixed",
        width: "60px",
        height: "60px",
        bottom: "40px",
        right: "40px"
      }}
      size="large"
      color="primary"
      aria-label="add"
      component={React.forwardRef((props, ref) => (
        <Link
          to={{
            pathname: fBProps.pathname,
            state: { referer: fBProps.from }
          }}
          {...props}
          ref={ref}
        />
      ))}
    >
      <AddIcon />
    </Fab>
  )
}
