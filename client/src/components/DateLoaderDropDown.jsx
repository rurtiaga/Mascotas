//Libreria para manejo de fechas
import "date-fns"
//Importo y uso el formato para hispanohablantes
import dateLocaleES from "date-fns/locale/es"
import React from "react"
import DateFnsUtils from "@date-io/date-fns"
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers"

export default function DateLoader(props) {
  const handleDate = date => {
    props.setSelectedDate(date)
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={dateLocaleES}>
      <KeyboardDatePicker
        autoOk
        disableFuture
        inputVariant="outlined"
        margin="normal"
        id="date-picker-dialog"
        label="Fecha"
        format="dd/MM/yyyy"
        value={props.selectedDate}
        onChange={handleDate}
        KeyboardButtonProps={{
          "aria-label": "Seleccione una fecha"
        }}
      />
    </MuiPickersUtilsProvider>
  )
}
