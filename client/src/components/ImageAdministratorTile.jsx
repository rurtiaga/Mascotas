import React, { useEffect } from "react"
import { makeStyles } from "@material-ui/core/styles"
import GridList from "@material-ui/core/GridList"
import GridListTile from "@material-ui/core/GridListTile"
import GridListTileBar from "@material-ui/core/GridListTileBar"
import IconButton from "@material-ui/core/IconButton"
import StarFilledIcon from "@material-ui/icons/Star"
import StarBorderIcon from "@material-ui/icons/StarBorder"
import DeleteIcon from "@material-ui/icons/Delete"

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    flexWrap: "nowrap",
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: "translateZ(0)"
  },
  title: {
    color: theme.palette.primary.light
  },
  titleBar: {
    background:
      "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
  },
  titleTransparent: {
    background: "rgba(0,0,0,0) 100%"
  }
}))

/**
 * The example data is structured as follows:
 *
 * import image from 'path/to/image.jpg';
 * [etc...]
 *
 * const tileArray = [
 *   {
 *     img: image,
 *     title: 'Image',
 *     author: 'author',
 *     remove: funtion on remove,
 *     selectMain: fn to select this actual item,
 *   },
 *   {
 *     [etc...]
 *   },
 * ];
 */
export default function ImageAdministratorTile(props) {
  const classes = useStyles()
  const tileArray = props.tileData

  useEffect(() => {}, [tileArray])

  const handleStarClick = async tile => {
    try {
      await tile.selectMain()
      props.onSelect(tile)
    } catch (error) {
      console.log(error)
    }
  }

  const handleDelete = async tile => {
    try {
      await tile.remove()
      props.onDelete(tile.img)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div className={classes.root}>
      <GridList className={classes.gridList} cols={2.5}>
        {tileArray.map(tile => (
          <GridListTile key={tile.img}>
            <img src={tile.img} alt={tile.title} />
            <GridListTileBar
              title={tile.title}
              titlePosition="top"
              classes={{
                root: classes.titleTransparent,
                title: classes.title
              }}
              actionIcon={
                <IconButton
                  variant="contained"
                  color="secondary"
                  aria-label={`delete ${tile.title}`}
                  onClick={() => handleDelete(tile)}
                >
                  <DeleteIcon className={classes.title} />
                </IconButton>
              }
            />
            <GridListTileBar
              title={tile.title}
              classes={{
                root: classes.titleTransparent,
                title: classes.title
              }}
              actionIcon={
                tile === props.selectedMainPic ? (
                  <IconButton aria-label={`star ${tile.title}`}>
                    <StarFilledIcon className={classes.title} />
                  </IconButton>
                ) : (
                  <IconButton
                    aria-label={`star ${tile.title}`}
                    onClick={() => handleStarClick(tile)}
                  >
                    <StarBorderIcon className={classes.title} />
                  </IconButton>
                )
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
  )
}
