import React, { useContext, useEffect, useState } from "react"
import { makeStyles } from "@material-ui/core/styles"
import { Container, Box, Grid, Typography } from "@material-ui/core"
import Paper from "@material-ui/core/Paper"

import { AuthContext } from "../utils/context/Auth"
import uploadPet from "../routes/loadPet/request/uploadPet"
import uploadImages from "../routes/loadPet/request/uploadImages"
import PetFullScreen from "./PetFullScreen"
import SearchCard from "./cards/searchCard"
import BackAndConfirmNav from "./BackAndConfirmNav"

const useStyles = makeStyles(theme => ({
  sizing: {
    transform: `scale(0.7)`,
    transformOrigin: "top"
  }
}))

export default function LostFoundConfirm(props) {
  const routerState = props.location.state
  const { auth } = useContext(AuthContext)
  const classes = useStyles()
  let [imagesForPreview, setImagesForPreview] = useState([])

  const makeRequests = async () => {
    let idPet
    try {
      //trato de traerme la mascota del estado si es que existe, sinó la subo
      idPet = routerState.idPet
        ? routerState.idPet
        : await uploadPet(auth, routerState.pet)
      //subo las imagenes
      if (routerState.referer !== "/pet/edit") {
        await uploadImages(auth, idPet, routerState.images)
      }
      //subo la busqueda
      await props.uploadSearch(
        auth,
        idPet,
        routerState.pointLocation,
        routerState.date
      )
      goHome()
    } catch (error) {
      console.log(error)
    }
  }

  const goHome = () => {
    props.history.push({
      pathname: "/",
      state: {
        referer: `${routerState.referer}/confirm`
      }
    })
  }

  const backWithRouter = () => {
    props.history.push({
      pathname: `/${props.history.location.pathname.split("/")[1]}`,
      state: {
        ...props.location.state,
        referer: props.history.location.pathname
      }
    })
  }

  function readFileAsync(file) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader()

      reader.onload = e => {
        resolve(e.target.result)
      }

      reader.onerror = reject

      reader.readAsDataURL(file)
    })
  }

  useEffect(() => {
    const f = async () =>
      setImagesForPreview(
        typeof routerState.images[0] === "string"
          ? routerState.images
          : await Promise.all(
              routerState.images.map(async i => readFileAsync(i))
            )
      )
    f()
  }, [routerState])

  return (
    <Container>
      <Box mb="2em">
        <Typography variant="h3" align="center">
          Así se verá tu publicación:
        </Typography>
      </Box>
      <Grid container>
        <Grid item md={8}>
          <Box width="800px" height="600px" padding={2}>
            <Paper className={classes.sizing} m={2}>
              <PetFullScreen
                pet={{
                  ...routerState.pet,
                  pic: imagesForPreview.length > 0,
                  pics: imagesForPreview,
                  haveSearch: {
                    name:
                      props.location.pathname === "/lostPet/confirm"
                        ? "lost"
                        : "found"
                  },
                  date: routerState.date
                }}
              />
            </Paper>
          </Box>
        </Grid>

        <Grid item md={4}>
          <SearchCard
            pet={{
              ...routerState.pet,
              distance: 1,
              date: routerState.date,

              pic: imagesForPreview.length > 0,
              pics: imagesForPreview
            }}
          />
        </Grid>
      </Grid>
      <BackAndConfirmNav
        back={() => backWithRouter()}
        accept={() => makeRequests()}
        acceptText={"Confirmar"}
      />
    </Container>
  )
}
