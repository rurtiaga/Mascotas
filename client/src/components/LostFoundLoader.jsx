import React, { useState, useEffect } from "react"
import { Redirect } from "react-router-dom"
import { Container, Grid, Typography, Box } from "@material-ui/core"
import DateLoader from "./DateLoader"
import Map from "./Map"
import BackAndConfirmNav from "./BackAndConfirmNav"
import Loading from "./loading"

import { usePosition } from "../utils/usePosition"

function validLocation(location) {
  return location.lat && location.lng
}

export default function LostFoundLoader(props) {
  const state = props.location.state || {}

  const pet = state && state.pet ? state.pet : {}
  const images = state && state.images ? state.images : []
  const [date, setdate] = useState(
    state && state.date ? state.date : Date.now()
  )
  const { lat, lng } = usePosition()
  const [pointLocation, setPointLocation] = useState({})
  const [zoomLocation, setZoomLocation] = useState({})

  useEffect(() => {
    const loc =
      state && state.pointLocation ? state.pointLocation : { lat, lng }
    setZoomLocation(loc)
    setPointLocation(loc)
  }, [lat, lng, state])

  const backWithRouter = () => {
    props.history.push({
      pathname: "/loadPet",
      state: {
        pet,
        images,
        date,
        pointLocation,
        referer: props.referer
      }
    })
  }

  const toAcceptScreen = () => {
    props.history.push({
      pathname: `/${props.referer}/confirm`,
      state: {
        pet,
        images,
        date,
        pointLocation,
        referer: props.referer,
        ...state
      }
    })
  }

  if (!(state.pet || state.idPet)) {
    return (
      <Redirect
        to={{
          pathname: `/loadPet`,
          state: { referer: props.referer }
        }}
      />
    )
  } else {
    return (
      <Container component="main" maxWidth="md">
        {/* <Grid container align="center" justify="center"> */}
        <Box mb="2em">
          <Typography variant="h4" align="center">
            {props.title}
          </Typography>
        </Box>
        <Grid container align="center" justify="center">
          <Grid item>
            <DateLoader setSelectedDate={setdate} selectedDate={date} />
          </Grid>
          <Grid
            item
            md={6}
            style={{
              position: "relative",
              height: "600px",
              width: "600px"
            }}
          >
            {validLocation(zoomLocation) && validLocation(pointLocation) ? (
              <Map
                pointLocation={pointLocation}
                zoomLocation={zoomLocation}
                setPointLocation={setPointLocation}
                setZoomLocation={setZoomLocation}
              />
            ) : (
              <Box alignContent="center">
                <Loading />
                <Typography variant="h4">
                  Tratando de obtener localización
                </Typography>
                <Typography>
                  para mayor precision acepte la solicitud de localización
                </Typography>
              </Box>
            )}
          </Grid>
        </Grid>
        <BackAndConfirmNav
          back={() => backWithRouter()}
          accept={() => toAcceptScreen()}
        />
      </Container>
    )
  }
}
