import React, { useContext } from "react"
import { LostPetsContext } from "../utils/context/LostPets"
import Map from "./Map"

//El mapa que va en conjunto con la nav, posee la propiedad de ocultarse
export default function MapNav(props) {
  const { geoLocation, setGeoLocation } = useContext(LostPetsContext)
  if (props.hide) {
    return null
  }
  return (
    <div style={{ width: "100%", height: "400px", marginBottom: "1em" }}>
      <Map
        pointLocation={geoLocation}
        zoomLocation={geoLocation}
        setPointLocation={setGeoLocation}
      />
    </div>
  )
}
