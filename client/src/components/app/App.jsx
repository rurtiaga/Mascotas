import React from "react"
import { BrowserRouter as Router } from "react-router-dom"
import Routes from "../../routes"
import NavBar from "../navBar"
import ReactNotification from "react-notifications-component"
import { AuthProvider } from "../../utils/context/Auth"
import "react-notifications-component/dist/theme.css"
import theme from "../../themes"
import { ThemeProvider } from "@material-ui/styles"

function App() {
  return (
    <AuthProvider>
      <ReactNotification />

      <ThemeProvider theme={theme}>
        <Router>
          <div>
            <NavBar>
              <Routes />
            </NavBar>
          </div>
        </Router>
      </ThemeProvider>
    </AuthProvider>
  )
}

export default App
