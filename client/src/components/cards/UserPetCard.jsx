import React from "react"
import BaseCard from "./baseCard"
import { Typography } from "@material-ui/core"

function translate(name) {
  switch (name) {
    case "lost":
      return "Perdido"
    case "found":
      return "Encontré"
    default:
      return "Feliz"
  }
}

export default function UserPetCard(props) {
  return (
    <BaseCard
      pet={props.pet}
      toLink={{
        pathname: "/pet/edit",
        state: { petId: props.pet._id }
      }}
    >
      <Typography variant="body2" color="textSecondary">
        Estado:
        {props.pet.haveSearch && props.pet.haveSearch.name
          ? translate(props.pet.haveSearch.name)
          : "Feliz"}
      </Typography>
    </BaseCard>
  )
}
