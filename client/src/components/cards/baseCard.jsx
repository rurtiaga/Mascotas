import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import CardActionArea from "@material-ui/core/CardActionArea"
import CardActions from "@material-ui/core/CardActions"
import CardContent from "@material-ui/core/CardContent"
import CardMedia from "@material-ui/core/CardMedia"
import Typography from "@material-ui/core/Typography"
import Grid from "@material-ui/core/Grid"
import logo from "../../assets/images/search/search-logo.png"
import { Link } from "react-router-dom"

const useStyles = makeStyles({
  card: {
    maxWidth: 300,
    minWidth: 300
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  }
})

export default function BaseCard(baseCprops) {
  const classes = useStyles()
  const pet = baseCprops.pet

  return (
    <Grid item>
      <Card className={classes.card}>
        <CardActionArea
          component={React.forwardRef((props, ref) => (
            <Link to={baseCprops.toLink} {...props} ref={ref} />
          ))}
        >
          {pet.pic ? (
            <CardMedia
              className={classes.media}
              image={pet.picLink || pet.pics[0]}
            />
          ) : (
            <CardMedia className={classes.media} image={logo} />
          )}
          <CardContent>
            <Typography gutterBottom align="center" variant="h5" component="h2">
              {pet.name}
            </Typography>
            <Grid container spacing={1}>
              {baseCprops.children}
            </Grid>
          </CardContent>
        </CardActionArea>
        <CardActions>{baseCprops.buttons}</CardActions>
      </Card>
    </Grid>
  )
}
