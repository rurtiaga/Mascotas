import React from "react"
import Typography from "@material-ui/core/Typography"
import Grid from "@material-ui/core/Grid"
import BaseCard from "../baseCard"
import moment from "moment"
import translate from "../../../utils/translate"

export default function SearchCard(props) {
  const pet = props.pet

  return (
    <BaseCard
      pet={pet}
      toLink={{
        pathname: "/pet",
        state: { petId: pet.pet, referer: props.location }
      }}
    >
      <Grid container spacing={1}>
        <Grid item>
          <Typography variant="body2" color="textSecondary">
            Distancia: {pet.distance < 1 ? "< 1" : "~ " + pet.distance} km
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body2" color="textSecondary">
            Especie: {translate(pet.species)}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body2" color="textSecondary">
            Edad: {translate(pet.age)}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body2" color="textSecondary">
            Desde: {moment(pet.date).fromNow()}
          </Typography>
        </Grid>
      </Grid>
    </BaseCard>
  )
}
