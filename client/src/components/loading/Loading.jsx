import React from "react"
import CircularProgress from "@material-ui/core/CircularProgress"

export default function(params) {
  return (
    <div align="center">
      <CircularProgress />
    </div>
  )
}
