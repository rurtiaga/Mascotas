import React, { useState } from "react"
import { withStyles } from "@material-ui/core/styles"
import Button from "@material-ui/core/Button"
import Dialog from "@material-ui/core/Dialog"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import MuiDialogContent from "@material-ui/core/DialogContent"
import MuiDialogActions from "@material-ui/core/DialogActions"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import Typography from "@material-ui/core/Typography"
import obtainUserContactInfo from "../../routes/pet/request/obtainUserContactInfo"
import { Container } from "@material-ui/core"

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[700]
  }
})

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent)

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1)
  }
}))(MuiDialogActions)

export default function ContactButton(props) {
  const [open, setOpen] = useState(false)
  const [contactInfo, setContactInfo] = useState({})

  const handleClickOpen = async () => {
    const cI = await obtainUserContactInfo(props.petId)
    setContactInfo(cI)
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <Container>
      <Button
        variant="contained"
        color="primary"
        size="large"
        // className={classes.button}
        onClick={handleClickOpen}
      >
        Contactar
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Contacto
        </DialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>Nombre:{contactInfo.fName || ""}</Typography>
          <Typography gutterBottom>
            Email:{contactInfo.email || "No hay información"}
          </Typography>
          <Typography gutterBottom>
            Teléfono:{contactInfo.phone || "No hay información"}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
    </Container>
  )
}
