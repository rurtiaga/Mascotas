import React, { useContext } from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Divider from "@material-ui/core/Divider"
import { makeStyles } from "@material-ui/core/styles"
import { Container, Button } from "@material-ui/core"
import SharePanel from "./SharePanel"
import ContactButtonModal from "./ContactButtonModal"
import deletePetSearch from "../stateEditor/deletePetSearch"
import deletePetFoundSearch from "../stateEditor/deletePetFoundSearch"
import { AuthContext } from "../../utils/context/Auth"
import { Link } from "react-router-dom"
import moment from "moment"
import translate from "../../utils/translate"

const useStyles = makeStyles(theme => ({
  section1: {
    margin: theme.spacing(3, 2)
  },
  section2: {
    margin: theme.spacing(2, 1, 1)
  },
  name: {
    margin: theme.spacing(1)
  }
}))

export default function PanelInfo(props) {
  const { auth } = useContext(AuthContext)
  const classes = useStyles()

  const redirectToProfile = () =>
    props.history.push({
      pathname: "/profile/pets",
      state: {
        referer: props.location.pathname
      }
    })

  const handlePetAppears = () => {
    switch (props.pet.haveSearch.name) {
      case "lost":
        return deletePetSearch(auth.user, props.pet._id, () => {
          redirectToProfile()
          console.log("Apareció")
        })
      case "found":
        return deletePetFoundSearch(auth.user, props.pet._id, () => {
          redirectToProfile()
          console.log("Se encontró a su dueño")
        })
      default:
        console.log("no search")
        break
    }
  }

  return (
    <Container>
      <Grid container>
        <Grid item>
          <Typography variant="h3" className={classes.section1}>
            {props.pet.haveSearch && props.pet.haveSearch.name === "lost"
              ? "Perdido"
              : "Buscando al dueño"}
          </Typography>
        </Grid>
        <Grid item>
          {props.myPet && (
            <div>
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={handlePetAppears}
              >
                {props.pet.haveSearch && props.pet.haveSearch.name === "lost"
                  ? "Apareció"
                  : "Encontrado"}
              </Button>
              <Button
                variant="contained"
                color="primary"
                size="large"
                component={React.forwardRef((fProps, ref) => (
                  <Link
                    to={{
                      pathname: "/pet/edit",
                      state: {
                        petId: props.pet._id
                      }
                    }}
                    {...fProps}
                    ref={ref}
                  />
                ))}
              >
                Editar
              </Button>
            </div>
          )}
        </Grid>
      </Grid>
      <Divider variant="middle" />
      <div>
        <Grid container>
          <Grid item xs={12}>
            <Typography
              variant="h6"
              color="textPrimary"
              className={classes.section2}
            >
              Nombre:
            </Typography>
          </Grid>
          <Grid item xs={12} align="center">
            <Typography
              variant="h4"
              color="textPrimary"
              className={classes.name}
            >
              {props.pet.name}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="body1"
              color="textSecondary"
              className={classes.section2}
            >
              Desde:
              {moment(props.pet.date).fromNow()}
            </Typography>
          </Grid>

          <Grid item xs={6}>
            <Typography
              variant="body1"
              color="textSecondary"
              className={classes.section2}
            >
              Especie:{translate(props.pet.species)}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="body1"
              color="textSecondary"
              className={classes.section2}
            >
              Edad:{translate(props.pet.age)}
            </Typography>
          </Grid>
        </Grid>
      </div>

      <SharePanel />

      <Typography variant="body1" color="textSecondary">
        Descripción:{" "}
      </Typography>

      <Typography variant="body1" color="textSecondary">
        {props.pet.description}
      </Typography>

      <ContactButtonModal petId={props.pet._id} />
    </Container>
  )
}
