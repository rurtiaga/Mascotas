import React, { useContext, useState, useEffect } from "react"
import { LostPetsContext } from "../../utils/context/LostPets"
import getPetsSearchsFromGeo from "./request/getPetsSerachsFromGeo"
import getPetsFoundFromGeo from "./request/getPetsFoundFromGeo"
import SearchCard from "../cards/searchCard"
import Loading from "../loading"

import Grid from "@material-ui/core/Grid"
import { Typography, Box } from "@material-ui/core"

export default function(props) {
  const { geoLocation } = useContext(LostPetsContext)
  const [pets, setPets] = useState()

  const nameScreen = () => {
    switch (props.screen) {
      case "found":
        return "encontradas"
      default:
        return "perdidas"
    }
  }

  useEffect(() => {
    const search = gl => {
      switch (props.screen) {
        case "lost":
          return getPetsSearchsFromGeo(gl)
        case "found":
          return getPetsFoundFromGeo(gl)
        default:
          return getPetsSearchsFromGeo(gl)
      }
    }
    async function fetchData() {
      let res = await search(geoLocation)

      if (res && res.length >= 0) {
        setPets(res)
      }
    }
    fetchData()
    return
  }, [props.screen, geoLocation])

  if (pets) {
    if (pets.length > 0) {
      return (
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
          spacing={3}
        >
          {pets.map(p => (
            <SearchCard pet={p} key={p._id} />
          ))}
        </Grid>
      )
    } else {
      return (
        <Box mt={3}>
          <Typography align="center" variant="h3">
            No hay mascotas {nameScreen()}
          </Typography>
        </Box>
      )
    }
  }
  return (
    <Box mt={3}>
      <Loading />
      <Typography align="center" variant="h3">
        Buscando
      </Typography>
    </Box>
  )
}
