import React, { useEffect, useContext, useState } from "react"
import { Grid, Typography, Button } from "@material-ui/core"
import DateLoaderDropDown from "../DateLoaderDropDown"
import Map from "../Map"
import obtainLostPet from "./obtainLostPet"
import obtainFoundPet from "./obtainFoundPet"
import { AuthContext } from "../../utils/context/Auth"
import deletePetSearch from "./deletePetSearch"
import deletePetFoundSearch from "./deletePetFoundSearch"
import pushLostPetModifies from "./pushLostPetModifies"

function LostPet(props) {
  const handlePetAppears = () => {
    deletePetSearch(props.user, props.petId, () => props.cb())
  }

  return (
    <>
      <Grid item xs={8} md={7}>
        <Typography variant="h5">PERDIDO</Typography>
      </Grid>
      <Grid item xs={4} md={5}>
        <Button variant="contained" color="primary" onClick={handlePetAppears}>
          Apareció
        </Button>
      </Grid>
    </>
  )
}

function FoundPet(props) {
  const handlePetAppears = () => {
    deletePetFoundSearch(props.user, props.petId, () => props.cb())
  }

  return (
    <>
      <Grid item xs={8} md={7}>
        <Typography variant="h5">ENCONTRÉ</Typography>
      </Grid>
      <Grid item xs={4} md={5}>
        <Button variant="contained" color="primary" onClick={handlePetAppears}>
          Devuelto
        </Button>
      </Grid>
    </>
  )
}

export default function StateEditor(props) {
  const { auth } = useContext(AuthContext)
  let [petId] = useState(props.petId)
  const [date, setDate] = useState()
  const [geoLocation, setGeoLocation] = useState()
  let searchName = props.search.name

  const submitChanges = () => {
    pushLostPetModifies(auth.user, props.petId, {
      date,
      geoLocation: [geoLocation.lat, geoLocation.lng]
    })
  }

  useEffect(() => {
    const obtainSearch = (u, id) => {
      switch (searchName) {
        case "found":
          return obtainFoundPet(u, id)

        default:
          return obtainLostPet(u, id)
      }
    }
    const fetchSearch = async () => {
      let search
      try {
        search = await obtainSearch(auth.user, petId)
        console.log("Busqueda recibida correctamente")
        setDate(search.date)
        setGeoLocation({
          lat: search.location.coordinates[0],
          lng: search.location.coordinates[1]
        })
      } catch (error) {
        console.log("Hubo un error trayendo la busqueda", error)
      }
    }
    if (auth && auth.user) {
      fetchSearch()
    }
  }, [auth, petId, searchName, setDate, setGeoLocation])

  return (
    <Grid container>
      {props.search.name === "lost" ? (
        <LostPet
          user={auth.user}
          petId={props.petId}
          cb={() => props.hideSearch()}
        />
      ) : (
        <FoundPet
          user={auth.user}
          petId={props.petId}
          cb={() => props.hideSearch()}
        />
      )}

      <Grid item xs={12} md={12}>
        <DateLoaderDropDown
          setSelectedDate={d => {
            setDate(d)
            submitChanges()
          }}
          selectedDate={date}
        />
      </Grid>
      <Grid item xs={12} md={12}>
        <div
          style={{
            width: "100%",
            height: "400px",
            marginBottom: "1em"
          }}
        >
          <Map
            pointLocation={geoLocation}
            zoomLocation={geoLocation}
            setPointLocation={gl => {
              setGeoLocation(gl)
              submitChanges()
            }}
          />
        </div>
      </Grid>
    </Grid>
  )
}
