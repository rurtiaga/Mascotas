import axios from "axios"
import { store } from "react-notifications-component"

export default async function(user, petId, cb) {
  let res
  try {
    res = await axios.delete(`/users/${user._id}/pets/${petId}/found`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: user.token
      }
    })
    console.log("Búsqueda de mascota borrada con éxito")
    store.addNotification({
      title: "Búsqueda Borrada con éxito",
      message: "la búsqueda de la mascota se han borrado",
      type: "success",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
  } catch (error) {
    console.log(error)
    store.addNotification({
      title: "Error borrando la búsqueda de mascota",
      message: "hubo un problema borrando la búsqueda de mascota",
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
    return error
  }
  cb()
  return res.data
}
