import axios from "axios"
import { store } from "react-notifications-component"

export default async function(user, petId, searchData) {
  let res
  try {
    res = await axios.put(
      `/pets/${petId}/search`,
      { search: searchData },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: user.token
        }
      }
    )
    console.log("Búsqueda de mascota modificada correctamente")
    store.addNotification({
      title: "Búsqueda de mascota modificados",
      message: "la búsqueda de la mascota se han modificado correctamente",
      type: "success",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
  } catch (error) {
    console.log(error)
    store.addNotification({
      title: "Error modificando la búsqueda de mascota",
      message: "hubo un problema modificando la la búsqueda de mascota",
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
    return error
  }
  return res.data
}
