import React, { useState, useEffect, useContext } from "react"
import { makeStyles } from "@material-ui/styles"

import { UsersToolbar, UsersTable } from "./components"
import getUsers from "./obtainUsers"
import { AuthContext } from "../../utils/context/Auth"
import { Grid, Typography } from "@material-ui/core"
import Loading from "../loading"

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}))

const UserList = () => {
  const classes = useStyles()
  const { auth } = useContext(AuthContext)

  const [users, setUsers] = useState([])
  const [cantUsers, setCantUsers] = useState()

  useEffect(() => {
    async function fetchData() {
      let res = await getUsers(auth.user)

      if (res && res.cantUsers >= 0) {
        setUsers(res.users)
        setCantUsers(res.cantUsers)
      }
    }
    fetchData()
  }, [auth])

  if (!cantUsers) {
    return <Loading />
  }

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={6}>
          <Typography>Cantidad de usuarios: {cantUsers}</Typography>
        </Grid>

        <Grid item xs={6}>
          <UsersToolbar />
        </Grid>
      </Grid>
      <div className={classes.content}>
        <UsersTable users={users} />
      </div>
    </div>
  )
}

export default UserList
