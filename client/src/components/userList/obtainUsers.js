import axios from "axios"
export default async function getUsers(user) {
  let res
  try {
    res = await axios.get("/users", {
      headers: {
        Authorization: user.token
      }
    })
  } catch (error) {
    console.log(error)
    //TODO
    return error
  }
  return res.data
}
