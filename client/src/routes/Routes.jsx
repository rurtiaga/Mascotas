import React, { Fragment, useContext } from "react"
import { Route, Redirect } from "react-router-dom"
import Home from "./home"
import LostPets from "./lostPets"
import Pet from "./pet"
import LoadPet from "./loadPet/LoadPet"
import LostPet from "./lostPet/LostPet"
import ConfirmLostPet from "./lostPet/confirm/ConfirmLostPet"
import Profile from "./profile/Profile"
import MyPets from "./profile/pets/MyPets"
import EditPet from "./pet/edit/EditPet"
import SignIn from "./singIn/signIn"
import Register from "./register/Register"
import ForgotPass from "./forgotPass/ForgotPass"
import ConfirmForgotPass from "./forgotPass/ConfirmForgotPass"
import RestorePass from "./restorePass/RestorePass"
import { AuthContext } from "../utils/context/Auth"
import ConfirmLoadPet from "./loadPet/confirm/ConfirmLoadPet"
import ConfirmFoundPet from "./foundPet/ConfirmFoundPet"
import FoundPet from "./foundPet/FoundPet"
import FoundPets from "./foundPets/FoundPets"
import UserList from "./users/UsersList"
import ImagesUpload from "./pet/uploadImages/ImagesUpload"

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { auth } = useContext(AuthContext)
  return (
    <Route
      {...rest}
      render={props =>
        !!auth ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/signIn",
              state: {
                ...props.location.state,
                referer: props.location.pathname
              }
            }}
          />
        )
      }
    />
  )
}

const AdminRoute = ({ component: Component, ...rest }) => {
  const { auth } = useContext(AuthContext)
  return (
    <Route
      {...rest}
      render={props =>
        !!auth ? (
          auth.user.rol === "Admin" ? (
            <Component {...props} />
          ) : (
            <span>No tiene permisos para hacer esto</span>
          )
        ) : (
          <Redirect
            to={{
              pathname: "/signIn",
              state: {
                ...props.location.state,
                referer: props.location.pathname
              }
            }}
          />
        )
      }
    />
  )
}

export default props => {
  return (
    <Fragment>
      <AdminRoute exact path="/usersList" component={UserList} />

      <Route exact path="/" component={Home} />
      <Route exact path="/signIn" component={SignIn} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/forgotPass" component={ForgotPass} />
      <Route exact path="/forgotPass/confirm" component={ConfirmForgotPass} />
      <Route exact path="/restore" component={RestorePass} />
      <Route path="/lostPets" component={LostPets} />
      <Route path="/foundPets" component={FoundPets} />
      <Route exact path="/pet" component={Pet} />
      <PrivateRoute path="/pet/edit" component={EditPet} />
      <Route exact path="/loadPet/" component={LoadPet} />
      <PrivateRoute path="/loadPet/confirm" component={ConfirmLoadPet} />
      <Route exact path="/foundPet/" component={FoundPet} />
      <PrivateRoute path="/foundPet/confirm" component={ConfirmFoundPet} />
      <Route exact path="/lostPet/" component={LostPet} />
      <PrivateRoute path="/lostPet/confirm" component={ConfirmLostPet} />
      <PrivateRoute exact path="/profile/" component={Profile} />
      <PrivateRoute exact path="/profile/pets" component={MyPets} />
      <PrivateRoute exact path="/pet/uploadImages" component={ImagesUpload} />
      {/* TODO for 404 <Route component={NoMatch}/> */}
    </Fragment>
  )
}
