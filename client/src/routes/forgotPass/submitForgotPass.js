import axios from "axios"
import { store } from "react-notifications-component"

export default email => {
  return axios
    .post(`/auth/forgot`, {
      email
    })
    .then(responseFromLogin => {
      store.addNotification({
        title: "Solicitud enviada con éxito",
        message:
          "En instantes le estaremos enviando un mail a su casilla de correo con un enlace, para la recuperación de su contraseña",
        type: "success",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 7000
        }
      })
      return responseFromLogin
    })
    .catch(e => {
      console.log("Falló solicitud de contraseña", e)
      store.addNotification({
        title: "No se pudo enviar la solicitud",
        message:
          "hay un problema enviando esta solicitud de restablecer la contraseña",
        type: "danger",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
    })
}
