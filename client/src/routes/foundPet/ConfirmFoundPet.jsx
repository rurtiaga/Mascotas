import React from "react"
import uploadConfirmFoundPet from "./uploadConfirmFoundPet"
import LostFoundConfirm from "../../components/LostFoundConfirm"

export default function ConfirmLostPet(props) {
  return <LostFoundConfirm {...props} uploadSearch={uploadConfirmFoundPet} />
}
