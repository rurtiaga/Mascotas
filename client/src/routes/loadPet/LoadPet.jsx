import React, { useState, useEffect } from "react"

import Chooser from "../../components/Chooser"
import { Button, Grid, TextField, Container } from "@material-ui/core"

import { DropzoneArea } from "material-ui-dropzone"
import BackAndConfirmNav from "../../components/BackAndConfirmNav"

function LoadPet(props) {
  const state = props.location.state
  const isFromBack = !!state && state.pet
  const [screenNum, setscreenNum] = useState(isFromBack ? 2 : 0)
  //pantalla 0
  const [species, setspecies] = useState(
    state && state.pet && state.pet.species ? state.pet.species : null
  )
  const [age, setage] = useState(
    state && state.pet && state.pet.age ? state.pet.age : null
  )
  const [sex, setsex] = useState(
    state && state.pet && state.pet.sex ? state.pet.sex : null
  )

  const allFieldsSelected = species && age && sex
  useEffect(() => {}, [species, age, sex])
  //pantalla 1
  const [description, setdescription] = useState(
    state && state.pet && state.pet.description ? state.pet.description : ""
  )
  const [name, setname] = useState(
    state && state.pet && state.pet.name ? state.pet.name : ""
  )

  //pantalla 2
  const [files, setfiles] = useState(state && state.images ? state.images : [])

  const nextScreen = () => {
    return setscreenNum(screenNum + 1)
  }

  const prevScreen = () => {
    return setscreenNum(screenNum - 1)
  }

  const acceptPet = () => {
    console.log("El state", state)
    if (!state || !state.referer) {
      props.history.push({
        pathname: "/signIn",
        state: {
          pet: { species, age, sex, description, name },
          images: files,
          referer: props.location.pathname
        }
      })
    } else {
      switch (state.referer) {
        case "lostPet":
          props.history.push({
            pathname: "/lostPet",
            state: {
              ...state,
              pet: { species, age, sex, description, name },
              images: files,
              referer: props.location.pathname
            }
          })
          break
        case "foundPet":
          props.history.push({
            pathname: "/foundPet",
            state: {
              ...state,
              pet: { species, age, sex, description, name },
              images: files,
              referer: props.location.pathname
            }
          })
          break
        default:
          props.history.push({
            pathname: "/LoadPet/confirm",
            state: {
              pet: { species, age, sex, description, name },
              images: files,
              referer: props.location.pathname
            }
          })
          break
      }
    }
  }

  switch (screenNum) {
    case 0:
      return (
        <Container component="main" maxWidth="md">
          <Grid container align="center" justify="center">
            <Chooser
              title="Especie:"
              options={[
                { key: "Cat", label: "Gato" },
                { key: "Dog", label: "Perro" }
              ]}
              selected={species}
              other="true"
              onSelect={setspecies}
            />
          </Grid>
          <Chooser
            title="Edad:"
            options={[
              { key: "Puppy", label: "Cachorro" },
              { key: "Young", label: "Joven" },
              { key: "Adult", label: "Adulto" },
              { key: "Old", label: "Anciano" }
            ]}
            selected={age}
            onSelect={setage}
          />
          <Chooser
            title="Sexo:"
            options={[
              { key: "Male", label: "Macho" },
              { key: "Female", label: "Hembra" },
              { key: "idk", label: "No sé" }
            ]}
            selected={sex}
            onSelect={setsex}
          />
          <div align="right">
            <Button
              variant="contained"
              color="primary"
              size="large"
              disabled={!allFieldsSelected}
              onClick={() => nextScreen()}
            >
              Siguiente
            </Button>
          </div>
        </Container>
      )
    case 1:
      return (
        <Container component="main" maxWidth="md">
          <Grid container align="center" justify="center">
            <Grid item align="left" xs={10}>
              <TextField
                label="Nombre"
                placeholder="Ingrese el nombre de la mascota"
                fullWidth
                margin="normal"
                variant="outlined"
                value={name}
                onChange={e => setname(e.target.value)}
              />
            </Grid>
          </Grid>

          <Grid container align="center" justify="center">
            <Grid item align="left" xs={10}>
              <TextField
                multiline
                fullWidth
                label="Descripción"
                placeholder="Describe la mascota"
                rows="10"
                value={description}
                onChange={e => setdescription(e.target.value)}
                margin="normal"
                variant="outlined"
              />
            </Grid>
          </Grid>
          <BackAndConfirmNav
            back={() => prevScreen()}
            accept={() => nextScreen()}
          />
        </Container>
      )
    case 2:
      return (
        <Container component="main" maxWidth="md">
          <Grid container align="center" justify="center">
            <Grid item xs={10}>
              <DropzoneArea
                initialFiles={files}
                onChange={setfiles}
                acceptedFiles={["image/*"]}
                filesLimit={5}
                dropzoneText={
                  "Arrastre las imágenes de  la mascota aquí o haga click"
                }
              />
            </Grid>
          </Grid>
          <BackAndConfirmNav
            back={() => prevScreen()}
            accept={() => acceptPet()}
          />
        </Container>
      )

    default:
      return null
  }
}

export default LoadPet
