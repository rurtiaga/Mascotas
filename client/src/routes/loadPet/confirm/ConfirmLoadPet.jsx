import React, { useContext } from "react"
import { Container, Grid, Box, Typography } from "@material-ui/core"
import BackAndConfirmNav from "../../../components/BackAndConfirmNav"
import { AuthContext } from "../../../utils/context/Auth"
import uploadPet from "../request/uploadPet"
import uploadImages from "../request/uploadImages"
import translate from "../../../utils/translate"

export default function ConfirmLoadPet(props) {
  const routerState = props.location.state
  const { auth } = useContext(AuthContext)

  const makeRequests = async () => {
    let idPet
    try {
      idPet = await uploadPet(auth, routerState.pet)
      //subo las imagenes
      await uploadImages(auth, idPet, routerState.images)
      goHome()
    } catch (error) {
      console.log(error)
    }
  }
  const goHome = () => {
    props.history.push({
      pathname: "/",
      state: {
        referer: "loadPet/confirm"
      }
    })
  }

  const backWithRouter = () => {
    props.history.goBack()
  }

  return (
    <Container>
      <Box mb="2em">
        <Typography variant="h3" align="center">
          Datos de tu mascota:
        </Typography>
      </Box>
      <Grid container>
        <Grid item>
          <Typography>
            Nombre: {routerState.pet.name || "Aún no tiene nombre"}
          </Typography>

          <Typography>
            Sexo: {translate(routerState.pet.sex) || "No se ha indicado sexo"}
          </Typography>

          <Typography>
            Edad: {translate(routerState.pet.age) || "No se ha indicado edad"}
          </Typography>

          <Typography>
            Especie:{" "}
            {translate(routerState.pet.species) ||
              "No hay especie especificada"}
          </Typography>

          <Typography>
            Descripción: {routerState.pet.description || "No hay descripción"}
          </Typography>
        </Grid>
      </Grid>
      <BackAndConfirmNav
        back={() => backWithRouter()}
        accept={() => makeRequests()}
        acceptText={"Confirmar"}
      />
    </Container>
  )
}
