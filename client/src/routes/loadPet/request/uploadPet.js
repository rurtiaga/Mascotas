import axios from "axios"
import { store } from "react-notifications-component"

export default (user, pet) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: user.user.token
    }
  }
  return axios
    .post(
      `/users/${user.user._id}/pets`,
      {
        pet
      },
      config
    )
    .then(r => {
      if (r.status === 201) {
        console.log("Mascota creada correctamente", r)
        store.addNotification({
          title: "Mascota creada correctamente",
          message: "mascota creada correctamente",
          type: "success",
          insert: "top",
          container: "bottom-left",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 5000
          }
        })
      } else {
        throw Error("Error en la creación de la mascota")
      }
      return r.data.id
    })
    .catch(e => {
      console.log(e)
      store.addNotification({
        title: "no se pudo crear la mascota",
        message: "hubo un problema en el servidor creando la mascota",
        type: "danger",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
      return e
    })
}
