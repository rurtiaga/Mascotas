import React from "react"
import LostFoundLoader from "../../components/LostFoundLoader"

export default function LostPet(props) {
  return (
    <LostFoundLoader
      title="¿Cuando y donde se perdió?"
      referer="lostPet"
      {...props}
    />
  )
}
