import React from "react"
import uploadSearch from "../request/uploadSearch"
import LostFoundConfirm from "../../../components/LostFoundConfirm"

export default function ConfirmLostPet(props) {
  return <LostFoundConfirm {...props} uploadSearch={uploadSearch} />
}
