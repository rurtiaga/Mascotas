import axios from "axios"
import { store } from "react-notifications-component"

export default (user, pet, location, date) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: user.user.token
    }
  }

  return axios
    .post(
      `/users/${user.user._id}/pets/${pet}/search`,
      {
        location: {
          type: "Point",
          coordinates: [location.lat, location.lng]
        },
        date
      },
      config
    )
    .then(r => {
      if (r.status === 201) {
        console.log("Búsqueda creada correctamente")
        store.addNotification({
          title: "Búsqueda creada",
          message: `La búsqueda se creo correctamente`,
          type: "success",
          insert: "top",
          container: "bottom-left",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 5000
          }
        })
      } else {
        throw Error("hubo un error creando la búsqueda")
      }
      return r.data.id
    })
    .catch(e => {
      console.log("no se pudo crear la búsqueda", e)
      store.addNotification({
        title: "Error creando la búsqueda",
        message: "hubo un problema creando la búsqueda",
        type: "danger",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
    })
}
