import React, { useEffect, useState, useContext } from "react"
import obtainPet from "./request/obtainPet"
import Loading from "../../components/loading"
import PetFullScreen from "../../components/PetFullScreen"
import { AuthContext } from "../../utils/context/Auth"

function Pet(props) {
  const { auth } = useContext(AuthContext)
  const [pet, setPet] = useState()
  const [myPet, setMyPet] = useState(false)
  let petId = props.location.state.petId
  useEffect(() => {
    const fetchPetData = async () => {
      let res = await obtainPet(petId, auth && auth.user)
      setPet(res)
      setMyPet(res.isMyPet)
    }
    fetchPetData()
  }, [auth, petId])
  //Mientras hace la busqueda pongo un Loading
  if (!pet) {
    return <Loading />
  }
  return <PetFullScreen pet={pet} myPet={myPet} {...props} />
}

export default Pet
