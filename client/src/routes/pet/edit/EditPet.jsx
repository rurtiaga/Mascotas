import React, { useState, useContext, useEffect, useCallback } from "react"
import {
  Grid,
  Container,
  Button,
  TextField,
  Select,
  FormControl,
  InputLabel,
  Typography
} from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import DeleteIcon from "@material-ui/icons/Delete"
import obtainPetInformationForEdit from "./obtainPetInformationForEdit"
import { AuthContext } from "../../../utils/context/Auth"
import ImageAdministratorTile from "../../../components/ImageAdministratorTile"
import pushPetModifies from "./pushPetModifies.js"
import changeSelectMain from "./changeSelectMain.js"

import BackAndConfirmNav from "../../../components/BackAndConfirmNav"
import StateEditor from "../../../components/stateEditor/StateEditor"
import removePet from "./removePet"
import { useToggle } from "../../../utils/useToggle"
import { Link } from "react-router-dom"
import Loading from "../../../components/loading"

import removeImage from "./removeImage"

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1),
    minWidth: 120
  }
}))

export default function EditPet(props) {
  const classes = useStyles()
  const { auth } = useContext(AuthContext)
  const [petFromResponse, setPetFromResponse] = useState()
  const [age, setAge] = useState("")
  const [name, setName] = useState("")
  const [species, setSpecies] = useState("")
  const [sex, setSex] = useState("")
  const [description, setDescription] = useState("")
  //lista de las imagenes en el formato para el visor de tiles
  const [listOfTiles, setListOfTiles] = useState([])
  const [selectedMainPic, setSelectedMainPic] = useState()

  let petId = props.location.state.petId

  //se usa para ocultar la busqueda de la mascota una vez que se borró
  const { on, toggle } = useToggle(true)

  const listWithOutTile = urlPic => {
    let copyList = [...listOfTiles]
    copyList.splice(
      copyList.findIndex(t => t.img === urlPic),
      1
    )
    return copyList
  }

  const makeTiles = useCallback(
    pics => {
      const parseName = url => {
        const urlArray = url.split("/")
        return urlArray[urlArray.length - 1]
      }

      return pics.map(urlPic => {
        const namePic = parseName(urlPic)
        return {
          img: urlPic,
          remove: async () => removeImage(auth.user, petId, namePic),
          selectMain: async () => changeSelectMain(auth.user, petId, namePic)
        }
      })
    },
    [auth, petId]
  )

  const petHaveSearch = () => {
    return petFromResponse && petFromResponse.haveSearch && on
  }

  const submitPetChanges = () => {
    pushPetModifies(auth.user, props.location.state.petId, {
      age,
      name,
      species,
      sex,
      description
    })
  }

  const handleAccept = () => {
    submitPetChanges()
  }

  const handleDeletePet = () => {
    //ventana de confirmación?
    removePet(auth.user, props.location.state.petId)
    props.history.goBack()
  }

  let handlePet = useCallback(
    petFromRequest => {
      setPetFromResponse(petFromRequest)
      setAge(petFromRequest.age)
      setName(petFromRequest.name)
      setSpecies(petFromRequest.species)
      setSex(petFromRequest.sex)
      setDescription(petFromRequest.description)
      const tiles = makeTiles(petFromRequest.pics)
      setListOfTiles(tiles)
      setSelectedMainPic((tiles && tiles[0]) || undefined)
    },
    [makeTiles]
  )

  useEffect(() => {
    const fetchPetInformation = async () => {
      handlePet(await obtainPetInformationForEdit(auth.user, petId))
    }

    if (auth && auth.user && !petFromResponse) {
      fetchPetInformation()
    }
  }, [auth, on, petId, selectedMainPic, handlePet, petFromResponse])

  if (!petFromResponse) {
    return <Loading />
  }

  return (
    <Container component="main" maxWidth="md">
      <Grid container justify="flex-start" spacing={2}>
        <Grid item xs={12} md={8} align="left">
          <Typography variant="h4" align="center">
            Editando Mascota
          </Typography>
        </Grid>
        <Grid item xs={12} md={4} align="right">
          <Button
            variant="contained"
            color="secondary"
            startIcon={<DeleteIcon />}
            onClick={handleDeletePet}
          >
            Borrar mascota
          </Button>
        </Grid>
        <Grid item xs={12} md={6}>
          <Grid container>
            <Grid item xs={12} md={12}>
              <ImageAdministratorTile
                tileData={listOfTiles}
                selectedMainPic={selectedMainPic}
                onDelete={urlPic => setListOfTiles(listWithOutTile(urlPic))}
                onSelect={tile => setSelectedMainPic(tile)}
                {...props}
              />

              <Button
                component={React.forwardRef((Lprops, ref) => (
                  <Link
                    to={{
                      pathname: "/pet/uploadImages",
                      state: {
                        petId: petId,
                        cantImages: listOfTiles.length,
                        referer: props.from
                      }
                    }}
                    {...Lprops}
                    ref={ref}
                  />
                ))}
              >
                Agregar Imagenes
              </Button>
            </Grid>
            <Grid container>
              <Grid item xs={12} md={12}>
                <TextField
                  label="Nombre"
                  placeholder="Nombre"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  value={name}
                  onChange={e => setName(e.target.value)}
                  className={classes.margin}
                />
              </Grid>
              <Grid item xs={4} md={4}>
                <FormControl variant="outlined" className={classes.margin}>
                  <InputLabel htmlFor="outlined-age-native-simple">
                    Edad
                  </InputLabel>
                  <Select
                    native
                    value={age}
                    onChange={event => setAge(event.target.value)}
                    inputProps={{
                      name: "age",
                      id: "outlined-age-native-simple"
                    }}
                  >
                    <option value="" />
                    <option value={"Puppy"}>Cachorra</option>
                    <option value={"Young"}>Joven</option>
                    <option value={"Adult"}>Adulta</option>
                    <option value={"Old"}>Anciana</option>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4} md={4}>
                <FormControl variant="outlined" className={classes.margin}>
                  <InputLabel htmlFor="outlined-species-native">
                    Especie
                  </InputLabel>
                  <Select
                    native
                    value={species}
                    onChange={event => setSpecies(event.target.value)}
                    inputProps={{
                      name: "species"
                    }}
                  >
                    <option value="" />
                    <option value={"Dog"}>Perro</option>
                    <option value={"Cat"}>Gato</option>
                    <option value={"Other"}>Otro</option>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4} md={4}>
                <FormControl variant="outlined" className={classes.margin}>
                  <InputLabel htmlFor="outlined-sex-native">Sexo</InputLabel>
                  <Select
                    native
                    value={sex}
                    onChange={event => setSex(event.target.value)}
                    inputProps={{
                      name: "sex"
                    }}
                  >
                    <option value="" />
                    <option value={"Female"}>Hembra</option>
                    <option value={"Male"}>Macho</option>
                    <option value={"Idk"}>No sé</option>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </Grid>

          <TextField
            label="Descripción"
            placeholder="Descripción"
            fullWidth
            multiline
            variant="outlined"
            value={description}
            rows={3}
            rowsMax={6}
            className={classes.margin}
            onChange={event => setDescription(event.target.value)}
          />
        </Grid>
        <Grid item align="center" xs={12} md={6}>
          {petHaveSearch() ? (
            <StateEditor
              search={petFromResponse.haveSearch}
              petId={props.location.state.petId}
              hideSearch={() => toggle()}
              // date={date}
              // setDate={d => setDate(d)}
              // geoLocation={geoLocation}
              // setGeoLocation={d => setGeoLocation(d)}
            />
          ) : (
            <>
              <Typography> no hay búsqueda para esta mascota</Typography>
              <Button
                color="primary"
                component={React.forwardRef((p, ref) => (
                  <Link
                    to={{
                      pathname: "/lostPet",
                      state: {
                        idPet: props.location.state.petId,
                        pet: { ...petFromResponse },
                        images: petFromResponse.pics,
                        referer: props.location.pathname
                      }
                    }}
                    {...p}
                    ref={ref}
                  />
                ))}
              >
                Perdí esta mascota
              </Button>

              <Typography>¿Me encontraste?</Typography>
              <Button
                color="primary"
                component={React.forwardRef((p, ref) => (
                  <Link
                    to={{
                      pathname: "/foundPet",
                      state: {
                        idPet: props.location.state.petId,
                        pet: { ...petFromResponse },
                        images: petFromResponse.pics,
                        referer: props.location.pathname
                      }
                    }}
                    {...p}
                    ref={ref}
                  />
                ))}
              >
                Encontré esta mascota
              </Button>
            </>
          )}
        </Grid>
        <BackAndConfirmNav accept={handleAccept} />
      </Grid>
    </Container>
  )
}
