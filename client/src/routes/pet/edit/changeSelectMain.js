import axios from "axios"

export default async function(user, petId, namePic) {
  let res
  try {
    res = await axios.get(
      `/users/${user._id}/pets/${petId}/mainpic/${namePic}`,
      {
        headers: {
          Authorization: user.token
        }
      }
    )
    console.log("se cambió la foto principal")
  } catch (error) {
    console.log(error.message, error)
    return error
  }
  return res.data
}
