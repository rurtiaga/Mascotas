import axios from "axios"

export default async function(user, petId) {
  let res
  try {
    res = await axios.get(`/users/${user._id}/pets/${petId}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: user.token
      }
    })
  } catch (error) {
    console.log(error)
    //TODO
    return error
  }
  return res.data
}
