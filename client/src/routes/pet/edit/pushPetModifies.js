import axios from "axios"
import { store } from "react-notifications-component"

export default async function(user, petId, newPetData) {
  let res
  try {
    res = await axios.put(
      `/users/${user._id}/pets/${petId}`,
      { pet: newPetData },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: user.token
        }
      }
    )
    console.log("Mascota modificada correctamente")
    store.addNotification({
      title: "Datos de mascota modificados",
      message: "los datos de la mascota se han modificado correctamente",
      type: "success",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
  } catch (error) {
    console.log(error)
    store.addNotification({
      title: "Error modificando la mascota",
      message: "hubo un problema modificando los datos de la mascota",
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
    return error
  }
  return res.data
}
