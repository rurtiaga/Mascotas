import axios from "axios"
import { store } from "react-notifications-component"

export default async function(user, petId, imageName) {
  let res
  try {
    res = await axios.delete(
      `/users/${user._id}/pets/${petId}/pics/${imageName}`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: user.token
        }
      }
    )
    console.log(`Imagen ${imageName} ha sido borrada`)
    store.addNotification({
      title: "Imagen borrada",
      message: "se borró la imagen",
      type: "success",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
  } catch (error) {
    console.log(error)
    store.addNotification({
      title: "No se pudo borrar la imagen",
      message:
        "intente de nuevo mas tarde, consulte el log para mas información",
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
    return error
  }
  return res.data
}
