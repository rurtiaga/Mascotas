import axios from "axios"
import { store } from "react-notifications-component"

export default async function(user, petId) {
  let res
  try {
    res = await axios.delete(`/users/${user._id}/pets/${petId}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: user.token
      }
    })
    console.log("La mascota se ha borrado correctamente")
    store.addNotification({
      title: "La mascota se ha borrado correctamente",
      message: "la mascota ha sido eliminada",
      type: "success",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
  } catch (error) {
    console.log(error)
    store.addNotification({
      title: "No se pudo borrar la mascota",
      message: "hubo un problema borrando la mascota",
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
    return error
  }
  return res.data
}
