import axios from "axios"
export default async function obtainPet(petId, user) {
  let res
  let config = user && {
    headers: {
      "Content-Type": "application/json",
      Authorization: user.token
    }
  }
  try {
    res = await axios.get(`/pets/${petId}`, config)
  } catch (error) {
    console.log(error)
    //TODO
    return error
  }
  return res.data
}
