import axios from "axios"
export default async function obtainUserContactInfo(petId) {
  let res
  try {
    res = await axios.get(`/pets/${petId}/contact`)
  } catch (error) {
    console.log(error)
    return error
  }
  return res.data
}
