import React, { useState, useContext } from "react"
import { Container, Grid } from "@material-ui/core"
import { DropzoneArea } from "material-ui-dropzone"
import BackAndConfirmNav from "../../../components/BackAndConfirmNav"
import uploadImages from "./uploadImages"
import { AuthContext } from "../../../utils/context/Auth"

export default function ImagesUpload(props) {
  const { auth } = useContext(AuthContext)
  const [files, setFiles] = useState([])

  const handleUpload = async () => {
    await uploadImages(auth.user, props.location.state.petId, files)
    props.history.push({
      pathname: "/pet/edit",
      state: {
        ...props.location.state,
        referer: props.location.pathname
      }
    })
  }

  return (
    <Container component="main" maxWidth="md">
      <Grid container align="center" justify="center">
        <Grid item xs={10}>
          <DropzoneArea
            onChange={setFiles}
            acceptedFiles={["image/*"]}
            filesLimit={5}
            dropzoneText={
              "Arrastre las imágenes de la mascota aquí o haga click"
            }
          />
        </Grid>
      </Grid>
      <BackAndConfirmNav accept={handleUpload} />
    </Container>
  )
}
