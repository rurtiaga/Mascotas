import axios from "axios"
import { store } from "react-notifications-component"

export default (user, idPet_, images) => {
  if (images.length === 0) {
    return
  }
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: user.token
    }
  }
  let bodyFormData = new FormData()
  images.forEach(filePath => {
    bodyFormData.append("pics", filePath)
  })

  return axios
    .put(`/users/${user._id}/pets/${idPet_}/pics`, bodyFormData, config)
    .then(r => {
      console.log("imagenes subidas correctamente", r)
      store.addNotification({
        title: "Imágenes subidas correctamente",
        message: "se subieron correctamente las imagenes",
        type: "success",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
      return r
    })
    .catch(e => {
      console.log(e)
      store.addNotification({
        title: "Error subiendo las imágenes",
        message: "hubo un error subiendo las imágenes",
        type: "danger",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
      return e
    })
}
