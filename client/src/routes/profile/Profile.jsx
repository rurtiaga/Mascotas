import React, { useState, useEffect, useContext } from "react"
import { Container, Grid, TextField, Box, Button } from "@material-ui/core"
import obtainProfile from "./obtainProfile.js"
// import obtainProfileImage from "./obtainProfileImage.js"
import pushProfileChanges from "./pushProfileChanges.js"
import Loading from "../../components/loading/index.js"
import { AuthContext } from "../../utils/context/Auth.jsx"
import SaveIcon from "@material-ui/icons/Save"

export default function User(props) {
  const { auth } = useContext(AuthContext)
  const [name, setName] = useState("")
  const [last_name, setLast_Name] = useState("")
  const [mail, setMail] = useState("")
  const [phone, setPhone] = useState("")
  const [image, setImage] = useState()

  const handleMyPets = () => {
    props.history.push({
      pathname: "/profile/pets",
      state: {
        referer: "profile"
      }
    })
  }

  const pushChanges = () => {
    pushProfileChanges(auth.user.token, { name, last_name, phone })
  }

  useEffect(() => {
    const fetchProfileData = async () => {
      let res = await obtainProfile(auth.user.token)
      // let resImage = await obtainProfileImage(auth.user.token)
      setName(res.user.name || "")
      setLast_Name(res.user.last_name || "")
      setMail(res.user.email || "")
      setPhone(res.user.phone || "")
      setImage(res.user.avatar)
    }

    if (auth && auth.user) {
      fetchProfileData()
    }
  }, [auth])

  //Mientras hace la busqueda pongo un Loading
  if (!auth && !name) {
    return <Loading />
  }

  return (
    <Container component="main" maxWidth="md">
      <Grid container justify="flex-start" spacing={2}>
        <Box clone order={{ xs: 2, md: 1 }}>
          <Grid item xs={12} md={6}>
            <TextField
              label="Nombre"
              placeholder="Nombre"
              fullWidth
              margin="normal"
              variant="outlined"
              value={name}
              onChange={e => setName(e.target.value)}
            />
            <TextField
              label="Apellido"
              placeholder="Apellido"
              fullWidth
              margin="normal"
              variant="outlined"
              value={last_name}
              onChange={e => setLast_Name(e.target.value)}
            />
            <TextField
              label="Mail"
              placeholder="Mail"
              fullWidth
              disabled
              margin="normal"
              variant="outlined"
              value={mail}
              onChange={e => setMail(e.target.value)}
            />
            <TextField
              label="Teléfono"
              placeholder="Teléfono"
              fullWidth
              margin="normal"
              variant="outlined"
              value={phone}
              onChange={e => setPhone(e.target.value)}
            />
          </Grid>
        </Box>
        <Box clone order={{ xs: 1, md: 2 }}>
          <Grid item align="center" xs={12} md={5}>
            <Grid container>
              <Grid item xs={12} md={12}>
                <Button
                  onClick={handleMyPets}
                  variant="contained"
                  color="primary"
                >
                  Mis Mascotas
                </Button>
                <Button
                  onClick={pushChanges}
                  variant="contained"
                  color="primary"
                  startIcon={<SaveIcon />}
                >
                  Guardar
                </Button>
              </Grid>

              <Grid item xs={12} md={12}>
                {/* //TODO imagen */}
                {image ? <img src={image} alt="imagen de perfil" /> : null}
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Grid>
    </Container>
  )
}
