import axios from "axios"

export default async function obtainPet(userToken) {
  let res
  try {
    res = await axios.get(`/me`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: userToken
      }
    })
    if (res.status === 200) {
      console.log("datos de usuario obtenidos con éxito")
    }
  } catch (error) {
    console.log(error)
    //TODO
    return error
  }
  return res.data
}
