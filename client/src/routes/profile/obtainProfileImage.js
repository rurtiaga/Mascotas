import axios from "axios"

export default async function obtainUserAvatar(userToken) {
  let res
  try {
    res = await axios.get(`/me/avatar`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: userToken
      }
    })
    if (res.status === 200) {
      console.log("imagen de usuario obtenida con éxito")
    }
  } catch (error) {
    console.log(error)
    return error
  }
  return res.data
}
