import React, { useContext, useEffect, useState } from "react"
import { Grid } from "@material-ui/core"
import obtainPetsUser from "./obtainPetsUser"
import { AuthContext } from "../../../utils/context/Auth"
import UserPetCard from "../../../components/cards/UserPetCard"
import NoPetsFoundPets from "./NoPetsFoundPets"
import Loading from "../../../components/loading"
import AddButton from "../../../components/Buttons/AddFloatButton"

export default function MyPets(props) {
  const { auth } = useContext(AuthContext)

  const [pets, setPets] = useState()

  useEffect(() => {
    const fetchPetsData = async () => {
      let res = await obtainPetsUser(auth.user.token, auth.user._id)
      setPets(res)
    }
    if (auth) fetchPetsData()
  }, [auth])

  if (!pets) {
    return <Loading />
  }
  if (pets.length !== 0) {
    return (
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="flex-start"
        spacing={3}
      >
        {pets.map(p => (
          <UserPetCard pet={p} key={p._id} />
        ))}
        <AddButton pathname={"/loadPet"} from={props.location.pathname} />
      </Grid>
    )
  } else {
    return (
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="flex-start"
        spacing={3}
      >
        <NoPetsFoundPets from={props.location.pathname} />
      </Grid>
    )
  }
}
