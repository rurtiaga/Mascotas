import React from "react"
import { Link } from "react-router-dom"
import {
  Container,
  CssBaseline,
  Typography,
  Button,
  Box
} from "@material-ui/core"

export default function NoPetsFoundPets(NPFProps) {
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box mt={3}>
        <Typography component="h1" variant="h5">
          No hay mascotas cargadas
        </Typography>
        <Button
          variant="contained"
          color="primary"
          component={React.forwardRef((props, ref) => (
            <Link
              to={{
                pathname: "/loadPet",
                state: { referer: NPFProps.from }
              }}
              {...props}
              ref={ref}
            />
          ))}
        >
          Nueva mascota
        </Button>
      </Box>
    </Container>
  )
}
