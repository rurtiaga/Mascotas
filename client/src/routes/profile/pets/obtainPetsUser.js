import axios from "axios"

export default async function obtainPetsUser(userToken, id) {
  let res
  try {
    res = await axios.get(`/users/${id}/pets`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: userToken
      }
    })
    if (res.status === 200) {
      console.log("datos de mascotas obtenidos con éxito")
    }
  } catch (error) {
    console.log(error)
    //TODO
    return error
  }
  return res.data
}
