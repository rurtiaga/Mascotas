import axios from "axios"
import { store } from "react-notifications-component"

export default async function pushProfileChanges(
  userToken,
  fieldsToBeModified
) {
  let res
  try {
    console.log(fieldsToBeModified)
    res = await axios.put(`/me`, fieldsToBeModified, {
      headers: {
        "Content-Type": "application/json",
        Authorization: userToken
      }
    })
    if (res.status === 200) {
      console.log("datos de usuario cargados con éxito")
      store.addNotification({
        title: "Perfil modificado",
        message: `El perfil se modificó correctamente`,
        type: "success",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
    }
  } catch (error) {
    console.log(error)
    store.addNotification({
      title: "Error modificando el perfil",
      message: `El perfil NO se modificó debido a un error`,
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
    return error
  }
  return res.data
}
