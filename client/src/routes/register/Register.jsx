import React, { useContext } from "react"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import CssBaseline from "@material-ui/core/CssBaseline"
import TextField from "@material-ui/core/TextField"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"
import Link from "@material-ui/core/Link"
import Grid from "@material-ui/core/Grid"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"

import { useForm } from "react-hook-form"
import { AuthContext } from "../../utils/context/Auth"
import submitRegister from "./submitRegister"

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

export default function SignUp(props) {
  const classes = useStyles()
  const { setAuth } = useContext(AuthContext)
  const { register, handleSubmit, errors } = useForm()

  const submit = async data => {
    try {
      await submitRegister(data, setAuth)
      redirectRouter()
    } catch (error) {
      console.log(error)
    }
  }

  //función para redireccionar después de registrarte, la idea que que después vuelva según el contexto
  const redirectRouter = () => {
    if (props.location.state && props.location.state.referer) {
      props.history.push({
        pathname: props.location.state.referer,
        state: {
          ...props.location.state,
          referer: props.location.pathname
        }
      })
    } else {
      props.history.push("/")
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Registrar
        </Typography>
        <form
          className={classes.form}
          noValidate
          onSubmit={handleSubmit(submit)}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fName"
                name="name"
                variant="outlined"
                required
                fullWidth
                error={!!errors.name}
                id="firstName"
                label="Nombre"
                inputRef={register({ required: true, pattern: /\D+/ })}
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                error={!!errors.last_name}
                id="lastName"
                label="Apellido"
                name="last_name"
                inputRef={register({ required: true, pattern: /\D+/ })}
                autoComplete="lName"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                error={!!errors.email}
                fullWidth
                id="email"
                label="Dirección de correo"
                name="email"
                inputRef={register({
                  required: true,
                  pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                })}
                autoComplete="email"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                error={!!errors.password}
                name="password"
                label="Contraseña"
                type="password"
                id="password"
                inputRef={register({ required: true })}
                autoComplete="current-password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                id="phone"
                label="Teléfono"
                name="phone"
                error={!!errors.phone}
                inputRef={register({
                  pattern: /\d+/
                })}
                autoComplete="phone"
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={
                  <Checkbox
                    value="allowExtraEmails"
                    color="primary"
                    checked
                    inputRef={register}
                  />
                }
                label="Quiero recibir información por mail. Tal como notificaciones"
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Registrar
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/signIn" variant="body2">
                ¿Ya tiene una cuenta? Ingrese aquí
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  )
}
