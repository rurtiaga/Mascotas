import axios from "axios"
import { store } from "react-notifications-component"

export default (user, setAuth) => {
  return axios
    .post(`/auth/register`, {
      user
    })
    .then(responseFromLogin => {
      setAuth(responseFromLogin.data)
      store.addNotification({
        title: "Registro exitoso",
        message: "Usted se ha registrado correctamente",
        type: "success",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
      return responseFromLogin
    })
    .catch(e => {
      console.log("Falló registro", e)
      store.addNotification({
        title: "No se ha podido registrar",
        message: "puede que haya un problema con los datos ingresados",
        type: "danger",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
    })
}
