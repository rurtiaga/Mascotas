import axios from "axios"
import { store } from "react-notifications-component"

export default async function sendRestorePassData(email, token, password) {
  let res
  try {
    res = await axios.put(`/auth/restore`, { email, token, password })
    if (res.status === 200) {
      console.log("Se ha cambiado la contraseña con éxito")
      store.addNotification({
        title: "Se ha cambiado la contraseña con éxito",
        message: `La contraseña se cambió éxitosamente ya puede ingresar con su nueva contraseña`,
        type: "success",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
    }
  } catch (error) {
    console.log(error)
    store.addNotification({
      title: "Error cambiando la contraseña",
      message: `no se pudo actualizar la contraseña debido a un error`,
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000
      }
    })
    return error
  }
  return res.data
}
