import React, { useContext } from "react"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import CssBaseline from "@material-ui/core/CssBaseline"
import TextField from "@material-ui/core/TextField"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"
import Grid from "@material-ui/core/Grid"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"

import { useForm } from "react-hook-form"
import submitSingIn from "./submitSingIn"
import { AuthContext } from "../../utils/context/Auth"
import { Link } from "react-router-dom"

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

export default function SignIn(props) {
  const classes = useStyles()
  const { setAuth } = useContext(AuthContext)
  const { register, handleSubmit } = useForm()

  const submit = async data => {
    try {
      await submitSingIn(data, setAuth)
      redirectRouter()
    } catch (error) {
      console.log(error)
    }
  }

  //función para redireccionar después de log in, vuelve según el contexto
  const redirectRouter = () => {
    if (props.location.state && props.location.state.referer) {
      props.history.push({
        pathname: props.location.state.referer,
        state: {
          ...props.location.state,
          referer: props.location.pathname
        }
      })
    } else {
      props.history.push("/")
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Ingresa
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit(submit)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Dirección de correo"
            name="email"
            autoComplete="email"
            inputRef={register({ required: true })}
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
            inputRef={register({ required: true })}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Recuérdeme"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Ingresar
          </Button>
          <Grid container>
            <Grid item xs>
              <Link
                to={{
                  pathname: "/forgotPass",
                  state: props.location.state
                }}
                variant="body2"
              >
                ¿Olvidó su contraseña?
              </Link>
            </Grid>
            <Grid item>
              <Link
                to={{
                  pathname: "/register",
                  state: props.location.state
                }}
                variant="body2"
              >
                ¿No tiene cuenta? Registrese aquí
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  )
}
