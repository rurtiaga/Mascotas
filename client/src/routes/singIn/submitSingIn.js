import axios from "axios"
import { store } from "react-notifications-component"

export default (user, setAuth) => {
  return axios
    .post(`/auth/login`, {
      user
    })
    .then(responseFromLogin => {
      setAuth(responseFromLogin.data)
      store.addNotification({
        title: "Ingreso exitoso",
        message: "Ha ingresado correctamente",
        type: "success",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
      return responseFromLogin
    })
    .catch(e => {
      console.log("No ha podido ingresar", e)
      store.addNotification({
        title: "No ha podido ingresar",
        message:
          "hubo un problema puede que el usuario o la contraseña no sean correctos",
        type: "danger",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
        }
      })
      throw e
    })
}
