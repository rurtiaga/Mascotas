import React from "react"
import Container from "@material-ui/core/Container"
import UserList from "../../components/userList"

export default function UsersList() {
  return (
    <Container component="main">
      <UserList />
    </Container>
  )
}
