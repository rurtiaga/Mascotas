import React, { createContext, useState } from "react"

const AuthContext = createContext()
const AuthConsumer = AuthContext.Consumer

const AuthProvider = props => {
  //el auth tiene  {_id,token,email}
  const [auth, setAuth] = useState()
  const contextValue = {
    auth,
    setAuth
  }
  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  )
}

export { AuthContext, AuthProvider, AuthConsumer }
