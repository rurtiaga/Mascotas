import { here } from "../../config"
import axios from "axios"

// import { usePlatform } from "here-maps-react";
//La librería esta implementando este uso
//   const platform = usePlatform({ app_code: here.code, app_id: here.id });
// const platform = new window.H.service.Platform({
//   app_id: here.id,
//   app_code: here.code
// });
const platform = new window.H.service.Platform({
  app_id: here.id,
  app_code: here.code
})

const geocoder = platform.getGeocodingService()

const searchHereText = function(keyboardInput, onResponseSearch, error) {
  geocoder.geocode(
    {
      searchText: keyboardInput
    },
    onResponseSearch,
    error ? error : e => alert(e)
  )
}

const searchHereGeo = function(geoLocation, onResponseSearch, error) {
  axios
    .get(
      `https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?xnlp=CL_JSMv3.0.17.0&app_id=${here.id}&app_code=${here.code}&prox=${geoLocation.lat}%2C${geoLocation.lng}%2C1000&mode=retrieveAreas&maxresults=1&gen=9`
    )
    .then(r => {
      onResponseSearch(r.data)
    })
    .catch(e => {
      error(e)
      alert(e)
    })
  // geocoder.reverseGeocode({
  //         prox: `${geoLocation.lat},${geoLocation.lng},1000`,
  //         mode: 'retrieveAreas',
  //         maxresults: '1',
  //         gen: '9'
  //     }, onResponseSearch, error ? error : e =>
  //     alert(e)
  // );
}

export { platform, geocoder, searchHereText, searchHereGeo }
