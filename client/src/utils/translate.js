export default params => {
  switch (params) {
    case "Cat":
      return "Gato"

    case "Dog":
      return "Perro"

    case "Male":
      return "Macho"

    case "Female":
      return "Hembra"

    case "Old":
      return "Viejo"

    case "Young":
      return "Joven"

    case "Puppy":
      return "Cachorro"

    case "Adult":
      return "Adulto"
    case "idk":
      return "No sé"

    default:
      return params
  }
}
