import { useEffect } from "react"
import { useGeoPositionHTML5 } from "./useGeoPositionHTML5"

export const usePosition = () => {
  const { lat, lng, setPosition, setError, error } = useGeoPositionHTML5()
  useEffect(() => {
    if (error) {
      fetch("https://ipapi.co/json")
        .then(res => res.json())
        .then(location =>
          setPosition({
            lat: location.latitude,
            lng: location.longitude
          })
        )
        .catch(e => {
          console.log("error getting ipapi location")
          setError("error getting ipapi location")
          //al no conseguir la ubicación pongo en buenos aires para que pueda aparecer el mapa
          setPosition({ lat: "-34.603722", lng: "-58.381592" })
        })
    }
  }, [error, setError, setPosition])
  return {
    lat,
    lng,
    error
  }
}
