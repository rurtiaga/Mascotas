## Docker comandos útiles

Ver los logs de un contenedor anteriores y en tiempo real.

    docker logs -f <CONTAINER>

para un entorno con docker-compose

    docker-compose logs -f
