const mongoose = require("mongoose")
const LostPet = mongoose.model("Lost_pet")
const FoundPet = mongoose.model("Found_pet")
const fs = require("fs")
const { Schema } = mongoose
const { SERVER_URL, PORT } = require("../../config")

let PetsSchema = new Schema(
  {
    name: String,
    species: String,
    sex: String, //male/female
    age: String,
    description: String,
    id_lost: {
      type: Schema.Types.ObjectId,
      ref: "Lost_pet"
    },
    id_found: {
      type: Schema.Types.ObjectId,
      ref: "Found_pet"
    },
    //lista con los nombres de los archivos de el resto de las fotos
    pics: [String],
    //nombre del archivo de la foto principal
    mainPic: String
  },
  {
    timestamps: true
  }
)

//Genera la url en donde esta desplegado actualmente el servidor con referencia a la foto por el id de mascota y su nombre
function UrlPetPicGenerator(id_pet, pic_name) {
  return `${SERVER_URL}:${PORT}/api/pets/${id_pet}/pics/${pic_name}`
}

async function saveSearchInDatabase(mongo_search_doc) {
  try {
    //hago efectivo los cambios en db
    return await mongo_search_doc.save()
  } catch (error) {
    console.log(error)
    throw error
  }
}

class Pet {
  /**
   * Metodos de instancia
   */

  async newLostPetSearch(location, date) {
    //Me fijo que no se pise con otro estado
    this.canPerformNewLostPet()

    //Si ya tenia una busqueda la borro
    if (this.id_lost) {
      await LostPet.findByIdAndDelete(this.id_lost)
    }

    let lost_pet_doc = await saveSearchInDatabase(
      new LostPet(this.newSearch(location, date))
    )

    //guardo el id de la busqueda en la mascota
    this.id_lost = lost_pet_doc.id
    console.log("Created NEW LOST PET SEARCH on pet", this._id)
    return lost_pet_doc._id
  }

  async newFoundPetSearch(location, date) {
    //Me fijo que no se pise con otro estado
    this.canPerformNewFoundPet()

    //Si ya tenia una busqueda la borro
    if (this.id_found) {
      await FoundPet.findByIdAndDelete(this.id_found)
    }

    let found_pet_doc = await saveSearchInDatabase(
      new FoundPet(this.newSearch(location, date))
    )

    //guardo el id de la busqueda en la mascota
    this.id_found = found_pet_doc.id
    console.log("Created NEW FOUND PET SEARCH on pet", this._id)
    return found_pet_doc._id
  }

  //Se fija si puede realizar una nueva Búsqueda de lost pet, en caso de que no tira error
  canPerformNewLostPet() {
    if (this.id_found) {
      throw { msj: "This pet have already a FOUND", status: 409 }
    }
  }
  canPerformNewFoundPet() {
    if (this.id_lost) {
      throw { msj: "This pet have already a LOST", status: 409 }
    }
  }

  //Crea un objeto con todos los datos necesarios para una búsqueda.
  newSearch(location, date) {
    return {
      pet: this._id,
      name: this.name,
      age: this.age,
      species: this.species,
      mainPicLocation: this.mainPic ? this.mainPicDir() : "",
      location: location,
      date: new Date(date)
    }
  }

  async editPet(dataPet) {
    await this.set(dataPet)

    //modifico los search
    if (this.id_lost) {
      await LostPet.findByIdAndUpdate(this.id_lost, {
        name: dataPet.name,
        age: dataPet.age,
        species: dataPet.species
      })
    }
    if (this.id_found) {
      await FoundPet.findByIdAndUpdate(this.id_found, {
        name: dataPet.name,
        age: dataPet.age,
        species: dataPet.species
      })
    }
    return this
  }

  //devuelve la mascota para visualizar del lado del cliente
  async toClient() {
    return {
      _id: this._id,
      name: this.name,
      description: this.description,
      species: this.species,
      sex: this.sex,
      age: this.age,
      pic: this.havePic(),
      pics: this.generateUrlPics(),
      //atributo que posee un objeto con el tipo de busqueda y el id o vacio si no tiene ninguna busqueda
      haveSearch: this.isInSearchState(),
      //si está perdido o fue encontrado desde hace cuanto tiempo
      date: await this.dateSearch()
    }
  }

  async dateSearch() {
    if (this.id_lost) {
      return await LostPet.getDate(this.id_lost)
    }
    if (this.id_found) {
      return await FoundPet.getDate(this.id_found)
    }
  }

  isInSearchState() {
    if (this.id_lost) {
      return { id: this.id_lost, name: "lost" }
    }
    if (this.id_found) {
      return { id: this.id_found, name: "found" }
    }
    return null
  }

  //devuelve si tiene alguna pic cargada
  havePic() {
    return !!(this.mainPic || this.pics > 0)
  }

  //retorna una lista con urls a las fotos de la mascota actual, la 0 es la principal
  generateUrlPics() {
    let res = []
    if (this.mainPic) {
      res.push(UrlPetPicGenerator(this._id, this.mainPic))
    }
    this.pics.forEach(pic_name => {
      res.push(UrlPetPicGenerator(this._id, pic_name))
    })
    return res
  }

  //estructura de carpetas donde se encuentran los diferentes archivos de la mascota actual
  picsDir() {
    return `./uploads/${this.parent().id}/${this.id}/`
  }

  //obtiene el path de un determinado archivo, si no lo puede generar por que no pertenece a esta mascota lanza un error
  getDirPic(pic_name) {
    if (this.mainPic != pic_name && !this.pics.some(pn => pn == pic_name)) {
      throw "no valid picname for this pet"
    }
    return this.picsDir() + pic_name
  }

  //obtiene el path de la foto principal de la mascota
  mainPicDir() {
    return this.getDirPic(this.mainPic)
  }

  //asigna el archivo para la foto principal, y borra la foto principal anterior
  setMainPic(fileName) {
    if (this.mainPic) {
      this.deleteImagePet(this.mainPic)
    }
    this.mainPic = fileName
    //Si posee un search actualizo la fotografia
    if (this.id_lost) {
      LostPet.updateMainPic(this.id_lost, this.mainPicDir())
    }
    if (this.id_found) {
      FoundPet.updateMainPic(this.id_found, this.mainPicDir())
    }
  }

  //devuelve el nombre de todos los archivos que posee esta mascota
  getAllPics() {
    return {
      principal: this.mainPic,
      pics: this.pics
    }
  }

  //setea la lista de archivos como la lista de picks, y borra las anteriores
  setPics(fileNames) {
    if (this.pics.length > 0) {
      this.deleteImagesPet(this.pics)
    }
    this.pics = fileNames
  }

  addPics(fileNames) {
    if (!this.mainPic && this.pics.length === 0) {
      this.setMainPic(fileNames.shift())
    }
    this.pics = this.pics.concat(fileNames)
  }

  deleteImagePet(fileName) {
    let dir = this.picsDir()
    //Borro el archivo
    fs.unlinkSync(dir + fileName)
    //Si esta la carpeta vacia la borro
    if (fs.readdirSync(dir) == 0) {
      fs.rmdirSync(dir)
    }
  }

  deleteImagesPet(arrayFilesNames) {
    arrayFilesNames.forEach(fileName => {
      this.deleteImagePet(fileName)
    })
  }

  //borra el archivo, si es que le corresponde a esta mascota
  removePic(id_pic) {
    this.deleteImagePet(id_pic)
    if (this.mainPic == id_pic) {
      this.actualiceMainPic()
      return true
    }
    if (this.pics.length == 0) {
      throw "pic reference not found"
    }
    let indexInPics = this.pics.findIndex(f => f == id_pic)
    if (indexInPics != -1) {
      this.pics.splice(indexInPics, 1)
    }
  }

  selectMainPicFromPics(id_pic) {
    if (this.mainPic == id_pic) {
      throw `${this.mainPic} is actually the main pic`
    }
    let indexInPics = this.pics.findIndex(f => f == id_pic)
    if (indexInPics === -1) {
      throw "this pic reference don't belong to this pet"
    }
    this.actualiceMainPic(indexInPics)
  }

  //Interno, modifica o borra la referencia en otros lugares de la foto principal de la mascota
  actualiceMainPic(picIndexToRemplaceMainPic) {
    let posiblePic = null
    if (this.pics.length > 0) {
      posiblePic =
        picIndexToRemplaceMainPic >= 0
          ? this.pics.splice(picIndexToRemplaceMainPic, 1, this.mainPic)[0]
          : this.pics.shift()
    }
    // borro la referencia a la imagen actual o actualizo si es que había una imagen
    this.mainPic = posiblePic
    //borro o actualizo la refencia a la imagen en los demás lugares
    if (this.id_lost) LostPet.updateMainPic(this.id_lost, this.mainPicDir())
    if (this.id_found) FoundPet.updateMainPic(this.id_found, this.mainPicDir())
  }

  //Borra la busqueda LOSTPET de la mascota actual, hay que pasarle el usuario
  async deleteSearch(user) {
    try {
      await LostPet.findByIdAndDelete(this.id_lost)
    } catch (error) {
      console.log(error)
    }
    this.id_lost = null
    if (user) {
      await user.save()
    }
  }

  //Borra la busqueda FOUNDPET de la mascota actual, hay que pasarle el usuario
  async deleteFoundPet(user) {
    try {
      await FoundPet.findByIdAndDelete(this.id_found)
    } catch (error) {
      console.log(error)
    }
    this.id_found = null
    if (user) {
      await user.save()
    }
  }

  //Borra todas las imagenes guardadas
  deleteAllImages() {
    this.deleteImagesPet(this.pics.concat([this.mainPic]))
  }

  preRemove() {
    try {
      this.deleteAllImages()
    } catch (error) {
      console.log("error borrando imagenes", error)
    }
    if (this.id_lost) LostPet.removeWithId(this.id_lost)

    if (this.id_found) FoundPet.removeWithId(this.id_found)
  }
}

PetsSchema.loadClass(Pet)

// //Agrega una función middleware antes de remover una mascota
//  //NO FUNCIONA si es un subdocument.
// PetsSchema.pre("remove", function(next) {
//     //remuevo las imagenes
//     console.log("****** antes de borrar")
//     this.deleteAllImages()
//     //remuevo la busqueda
//     LostPet.remove({ _id: this.search }).exec()
//     next()
// })

mongoose.model("Pets", PetsSchema)
module.exports = PetsSchema
