const mongoose = require("mongoose")
const { Schema } = mongoose
const { SERVER_URL, PORT } = require("../../config")

const pointSchema = new Schema({
  type: {
    type: String,
    enum: ["Point"],
    required: true
  },
  coordinates: {
    type: [Number],
    required: true
  }
})

var SearchesSchema = new Schema(
  {
    pet: {
      type: Schema.Types.ObjectId,
      ref: "Users.pets"
    },
    name: String,
    species: String,
    age: String,
    location: pointSchema,
    date: {
      type: Date,
      required: true
    },
    mainPicLocation: String
  },
  {
    timestamps: true
  }
)

class Search {
  static removeWithId(id_search) {
    this.deleteOne({ _id: id_search }).exec()
  }

  static updateMainPic(id_search, dir) {
    this.findById(id_search)
      .then(search => {
        search.mainPicLocation = dir
        return search.save()
      })
      .catch(e => {
        throw "No se pudo actualzar la direccion de la foto principal en search"
      })
  }

  static obtainMainPicDir(id_search) {
    return this.findById(id_search)
      .then(search => {
        if (!search) {
          throw Error("search not found")
        }
        if (!search.mainPicLocation) {
          throw Error("no image in this search")
        }
        return search.mainPicLocation
      })
      .catch(e => {
        throw Error("error encontrando el search")
      })
  }

  //recibe un objeto location con lat y long
  static obtainListOfSearchNearAtLocation1(location, search_type) {
    return this.aggregate([
      {
        $geoNear: {
          near: {
            type: "Point",
            coordinates: [parseFloat(location.lat), parseFloat(location.long)]
          },
          spherical: true,
          maxDistance: 5000,
          distanceField: "distance",
          distanceMultiplier: 0.001
        }
      },
      {
        $addFields: {
          pic: {
            $cond: [{ $eq: ["$mainPicLocation", ""] }, false, true]
          }
        }
      },
      {
        $project: {
          name: 1,
          age: 1,
          species: 1,
          pet: 1,
          pic: 1,
          date: 1,
          distance: {
            $round: ["$distance", 0]
          }
        }
      },
      {
        $addFields: {
          picLink: {
            $cond: {
              if: "$pic",
              then: {
                $concat: [
                  SERVER_URL,
                  ":",
                  {
                    $toString: PORT
                  },
                  `/api/search/${search_type}/`,
                  {
                    $toString: "$_id"
                  },
                  "/pic"
                ]
              },
              else: ""
            }
          }
        }
      }
    ])
  }

  static getDate(id) {
    return this.findById(id)
      .then(s => s.date)
      .catch(e => {
        throw Error("error trying to get date", e)
      })
  }

  static findByIdPet(id) {
    return this.findOne({
      pet: id
    }).select({
      pics: 1,
      pet: 1,
      name: 1,
      age: 1,
      species: 1,
      location: 1,
      date: 1
    })
  }
  static findByIdPetAndUpdate(id, updateData) {
    return this.updateOne(
      {
        pet: id
      },
      {
        date: updateData.date,
        location: {
          type: "Point",
          coordinates: updateData.geoLocation
        }
      }
    )
  }
}

SearchesSchema.index({
  location: "2dsphere"
})

class Lost extends Search {
  static obtainListOfSearchNearAtLocation(location) {
    return this.obtainListOfSearchNearAtLocation1(location, "l")
  }
}
class Found extends Search {
  static obtainListOfSearchNearAtLocation(location) {
    return this.obtainListOfSearchNearAtLocation1(location, "f")
  }
}

SearchesSchema.loadClass(Lost)

FoundSchema = SearchesSchema.clone()
FoundSchema.loadClass(Found)

mongoose.model("Lost_pet", SearchesSchema)
mongoose.model("Found_pet", FoundSchema)
