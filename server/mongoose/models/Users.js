const mongoose = require("mongoose")
const { Schema } = mongoose
var bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
var { jwt_token } = require("../../jwt_token")
var jwt_key = jwt_token()
const { SERVER_URL, PORT } = require("../../config")

const Pets = mongoose.model("Pets")
const PetsSchema = Pets.schema

var UsersSchema = new Schema(
  {
    rol: String,
    name: String,
    last_name: String,
    email: {
      type: String,
      unique: true,
      required: true
    },
    phone: Number,
    password: String,
    pets: [PetsSchema],
    avatar: Boolean,
    lastLogin: Date,
    // contact: {
    //     phone: Boolean,
    //     email: Boolean
    // },
    pass_reset: {
      token: String,
      expiration_date: Number
    }
  },
  {
    timestamps: true
  }
)

class Person {
  //recibe datos del usuario y el rol
  static createUser(user_data, rol) {
    return this.create(user_data).then(newUser => {
      newUser.setPassword(user_data.password)
      newUser.rol = rol
      newUser.save()
      return newUser
    })
  }
  //Crea un nuevo usuario
  static newUser(user_data) {
    return this.createUser(user_data, "User")
      .then(newUser => {
        return newUser.toAuthJSON()
      })
      .catch(err => {
        if (err.code == 11000) {
          return {
            errors: {
              message: "duplicated key for user",
              error: err
            }
          }
        }
        err.msj = "error saving the user"
        return err
      })
  }

  //NO EXPONER
  static newAdmin(user_data) {
    return this.createUser(user_data, "Admin")
      .then(newUser => {
        return newUser.toAuthJSON()
      })
      .catch(err => {
        if (err.code == 11000) {
          return {
            errors: {
              message: "duplicated key for user",
              error: err
            }
          }
        }
        err.msj = "error saving the user"
        return err
      })
  }

  /**
   * Crea un nuevo Lost pet para una mascota especifica
   * @param {id} id_user
   * @param {id} id_pet
   * @param {lat,lng} location
   * @param {Date} date
   */
  static async newSearch(id_pet, location, date) {
    let user = await this.findByPetId(id_pet)
    let pet = await user.pets.id(id_pet)
    let idSearch = await pet.newLostPetSearch(location, date)
    await user.save()
    return idSearch
  }

  static async newFoundSearch(id_pet, location, date) {
    let user = await this.findByPetId(id_pet)
    let pet = await user.pets.id(id_pet)
    let idSearch = await pet.newFoundPetSearch(location, date)
    await user.save()
    return idSearch
  }

  //Uso interno
  static findByPetId(id_pet) {
    return this.findOne({
      "pets._id": id_pet
    })
  }

  static async findByIdToClient(id_user) {
    const user = await this.findById(id_user)
    return user.toClient()
  }

  static async userHavePet(id_user, id_pet) {
    const user = await this.findById(id_user)
    return !!user.pets.id(id_pet)
  }

  static async toClientAdmin() {
    const users = await this.find({}).then(users =>
      users.map(u => u.toClient())
    )
    const cantUsers = await this.estimatedDocumentCount()
    return {
      users,
      cantUsers
    }
  }

  //Genera la url en donde esta desplegado actualmente el servidor con referencia a la foto por el id de mascota y su nombre
  urlAvatarGenerator() {
    return this.avatar
      ? `${SERVER_URL}:${PORT}/api/avatar/${this._id}`
      : undefined
  }

  //Datos para el cliente pero del lado privado
  toClient() {
    return {
      _id: this._id,
      name: this.name,
      last_name: this.last_name,
      phone: this.phone,
      email: this.email,
      avatar: this.urlAvatarGenerator(),
      petsInfo: {
        nPets: this.countPets(),
        nLost: this.countLostPets(),
        nFound: this.countFoundPets()
      },
      lastLogin: this.lastLogin
    }
  }

  static obtainFilePathFromIdPet(id_pet, namePic) {
    return this.findOne({
      "pets._id": id_pet
    })
      .then(async user => {
        return user.pets.id(id_pet).getDirPic(namePic)
      })
      .catch(e => {
        // e.msj = `error encontrando el path de la pic`
        console.log(e)
        return e
      })
  }

  static obtainFilePath(id, id_pet, id_pic) {
    return this.findById(id)
      .then(async user => {
        return user.pets.id(id_pet).getDirPic(id_pic)
      })
      .catch(e => {
        e.msj = "error trying to find id for user or pet"
        throw e
      })
  }

  /**Devuelve un objeto pet y un atributo es si el que lo solicitó es el dueño
   *
   * @param {id de la mascota} id
   * @param {id del usuario} userRequestId
   */
  static async findPetByIdPet(id, userRequestId) {
    let user
    let pet
    try {
      user = await this.findOne({
        "pets._id": id
      })
      pet = user.pets.id(id)
    } catch (error) {
      console.log(error)
      throw "unable to find this pet"
    }
    return { ...(await pet.toClient()), isMyPet: user.isMyId(userRequestId) }
  }

  //Authentication

  setPassword(password) {
    this.password = bcrypt.hashSync(password, 8)
  }

  validatePassword(password) {
    return bcrypt.compareSync(password, this.password)
  }

  generateJWT() {
    const today = new Date()
    const expirationDate = new Date(today)
    expirationDate.setDate(today.getDate() + 60)

    return jwt.sign(
      {
        email: this.email,
        id: this._id,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
        rol: this.rol
      },
      jwt_key
    )
  }

  toAuthJSON() {
    //guardo la ultima vez que se logeo
    this.lastLogin = Date.now()
    return {
      _id: this._id,
      name: this.name,
      last_name: this.last_name,
      avatar: this.urlAvatarGenerator(),
      email: this.email,
      token: this.generateJWT(),
      rol: this.rol
    }
  }

  isSameResetPassToken(token) {
    return this.pass_reset.token === token
  }

  static async isRegistered(email) {
    return (await this.getUserByEmail(email)).length === 1
  }

  //Devuelve una lista
  static getUserByEmail(email) {
    return this.find({
      email
    }).exec()
  }

  //Others

  getContactInfo() {
    let info = {}
    info.fName = this.getFullName()
    // if (this.phone && this.contact.phone) {
    info.phone = this.phone
    // }
    // if (this.email && this.contact.email) {
    info.email = this.email
    // }
    return info
  }

  getFullName() {
    return `${this.name} ${this.last_name}`
  }

  removeToken() {
    this.pass_reset = {}
  }

  addPet(pet) {
    let length = this.pets.push(pet)
    return this.pets[length - 1]._id
  }

  petsForClient() {
    return this.pets.map(p => p.toClient())
  }

  setAvatar(bool) {
    this.avatar = bool
  }

  findPetAndUpdate(id_pet, newDataPet) {
    const pet = this.pets.id(id_pet)
    return pet.editPet(newDataPet)
  }

  isMyId(id) {
    return this._id.toString() === id
  }

  countPets() {
    return this.pets.length
  }

  countLostPets() {
    return this.pets.filter(p => p.id_lost).length
  }

  countFoundPets() {
    return this.pets.filter(p => p.id_found).length
  }
}

UsersSchema.loadClass(Person)

mongoose.model("Users", UsersSchema)
