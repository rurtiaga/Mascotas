const avatarRouter = require("express").Router()

avatarRouter.get("/:id", async function(req, res, next) {
  //envia el archivo
  res.sendFile(`/uploads/${req.params.id}/avatar`, {
    root: "."
  })
})

module.exports = avatarRouter
