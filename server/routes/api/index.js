apiRouter = require("express").Router()
auth = require("../auth")

apiRouter.use("/auth", require("./auth"))
apiRouter.use("/lostpets", require("./lostpets"))
apiRouter.use("/foundpets", require("./foundPets"))
apiRouter.use("/me", require("./me"))
apiRouter.use("/users", require("./user"))
apiRouter.use("/avatar", require("./avatar"))
apiRouter.use("/pets", require("./pet"))
apiRouter.use("/search", require("./search"))

module.exports = apiRouter
