const mongoose = require("mongoose")
const Users = mongoose.model("Users")

module.exports = function(req, res, next) {
  return Users.findByIdToClient(req.userId)
    .then(user => {
      if (!user) {
        return next({ msj: "user not found", status: 404 })
      }

      return res.json({
        user: user
      })
    })
    .catch(e => next(e))
}
