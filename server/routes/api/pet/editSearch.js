const mongoose = require("mongoose")

const Searchs = mongoose.model("Lost_pet")
const Users = mongoose.model("Users")

module.exports = async function(req, res, next) {
  // me fijo si este usuario puede realizar la modificación
  if (!(await Users.userHavePet(req.userId, req.params.id))) {
    return next(new Error("Error esta pet no pertenece al usuario logeado"))
  }

  return Searchs.findByIdPetAndUpdate(req.params.id, req.body.search)
    .then(r => res.status(200).json(r))
    .catch(e => next(e))
}
