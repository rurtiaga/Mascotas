const petRouter = require("express").Router()
const auth = require("../../auth")

petRouter.route("/:id").get(auth.userData, require("./obtainPet"))

petRouter
  .route("/:id/search")
  .get(require("./obtainSearch"))
  .put(auth.required, require("./editSearch"))

petRouter
  .route("/:id/found")
  .get(require("./obtainFoundPet"))
  .put(auth.required, require("./editFound"))

petRouter.route("/:id/pics/:namePic").get(require("./pics/obtainPic"))

petRouter.route("/:id/contact").get(require("./contact"))

module.exports = petRouter
