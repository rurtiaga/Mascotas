const searchRouter = require("express").Router()

searchRouter.route("/l/:id/pic").get(require("./obtainMainPicLost"))
searchRouter.route("/f/:id/pic").get(require("./obtainMainPicFound"))

module.exports = searchRouter
