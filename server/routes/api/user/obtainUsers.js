const mongoose = require("mongoose")
const Users = mongoose.model("Users")

module.exports = function(req, res, next) {
  return Users.toClientAdmin()
    .then(users => {
      return res.json(users)
    })
    .catch(e => next(e))
}
