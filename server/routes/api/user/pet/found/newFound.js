var mongoose = require("mongoose")

const Users = mongoose.model("Users")

module.exports = function(req, res, next) {
  const id_pet = req.params.id_pet

  if (!(req.body.location || req.body.data)) {
    next(new Error("no data for create new search"))
  }

  //Como la mascota depende del user, y es responsabilidad de la
  // mascota crear el Search se debe acceder desde el User
  Users.newFoundSearch(id_pet, req.body.location, req.body.date)
    .then(id =>
      res.status(201).json({
        id
      })
    )
    .catch(e => {
      next(e)
    })
}
