const Users = require("mongoose").model("Users")

module.exports = function(req, res) {
  return Users.findPetByIdPet(req.params.id_pet)
    .then(r => res.status(200).json(r))
    .catch(e => res.sendStatus(500))
}
