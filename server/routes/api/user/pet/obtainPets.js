const Users = require("mongoose").model("Users")

module.exports = function(req, res, next) {
  Users.findById(req.params.id)
    .then(user => {
      if (!user) {
        return next({ msj: "no user", status: 400 })
      }
      return Promise.all(user.petsForClient())
    })
    .then(pets => res.status(200).json(pets))
    .catch(e => next(e))
}
