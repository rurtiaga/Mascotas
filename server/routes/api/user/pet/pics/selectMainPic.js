const Users = require("mongoose").model("Users")

module.exports = function(req, res, next) {
  const id_user = req.params.id
  const id_pet = req.params.id_pet
  const newMainPicRef = req.params.pic_ref

  Users.findById(id_user)
    .then(async user => {
      let pet = await user.pets.id(id_pet)
      await pet.selectMainPicFromPics(newMainPicRef)
      return user.save()
    })
    .then(() => res.sendStatus(200))
    .catch(e => next(e))
}
