const Users = require("mongoose").model("Users")

module.exports = function(req, res, next) {
  const id = req.userId
  const id_pet = req.params.id_pet
  let pics_filenames = []

  //check new extra pics
  if (req.files["pics"] && req.files["pics"].length > 0) {
    pics_filenames = req.files["pics"].map(f => f.filename)
  }

  Users.findById(id)
    .then(user => {
      if (!user) {
        next({ status: 400, msj: "user not found" })
      }
      pet = user.pets.id(id_pet)
      pet.addPics(pics_filenames)
      user.save()
      return "OK"
    })
    .then(r => {
      res.status(201).send(r)
    })
    .catch(e => {
      next(e)
    })
}
