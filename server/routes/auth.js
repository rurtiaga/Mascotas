var jwt = require("jsonwebtoken")
var { jwt_token } = require("../jwt_token")
jwt_key = jwt_token()

/**
 * Decodifica el token enviado se fija que exista, y que sea un token valido
 */
function verifyToken(req, res, next) {
  var token = req.headers["authorization"]
  if (!token)
    return res.status(403).send({
      auth: false,
      message: "No token provided."
    })
  jwt.verify(token, jwt_key, function(err, decoded) {
    if (err)
      return res.status(500).send({
        auth: false,
        message: "Failed to authenticate token."
      })
    // if everything good, save to request for use in other routes
    req.userId = decoded.id
    req.rol = decoded.rol
    next()
  })
}

/**
 * Requiere decodificar el token antes.
 *
 * Se fija que el id del usuario decodificado coincida con el enviado por params.
 */
function checkTokenId(req, res, next) {
  const id = req.params.id

  if (!id)
    return res.status(403).send({
      auth: false,
      message: "No token or id provided."
    })
  if (req.userId !== id) {
    return res.status(403).send({
      auth: false,
      message: "No rights for do this"
    })
  }
  next()
}

/**
 * Decodifica el token enviado y guarda la información en el req, si no hay token no hace nada.
 */
function tokenDecode(req, res, next) {
  var token = req.headers["authorization"]
  if (!token) {
    return next()
  }
  jwt.verify(token, jwt_key, function(err, decoded) {
    if (err)
      return res.status(500).send({
        auth: false,
        message: "Failed to authenticate token."
      })
    // if everything good, save to request for use in other routes
    req.userId = decoded.id
    next()
  })
}

/**
 * Requiere decodificar el token antes.
 *
 * Se fija que el rol del usuario decodificado sea administrador.
 */
function checkAdmin(req, res, next) {
  if (req.rol !== "Admin") {
    return res.status(403).send({
      auth: false,
      message: "No rights for do this"
    })
  }
  next()
}

const auth = {
  required: verifyToken,
  checkIdentity: checkTokenId,
  userData: tokenDecode,
  checkAdmin
}

module.exports = auth
