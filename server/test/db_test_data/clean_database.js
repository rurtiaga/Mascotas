const mongoose = require("mongoose")
const { MONGO_URL } = require("../../config")

async function cleanDB(params) {
  try {
    await mongoose.connect(MONGO_URL, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      // Old Server Discovery and Monitoring engine is deprecated this is for use the new one
      useUnifiedTopology: true,
      reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
      reconnectInterval: 500, // Reconnect every 500ms
      poolSize: 10, // Maintain up to 10 socket connections
      // If not connected, return errors immediately rather than waiting for reconnect
      bufferMaxEntries: 0,
      connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
      socketTimeoutMS: 45000 // Close sockets after 45 seconds of inactivity
    })
    await mongoose.connection.db
      .dropDatabase()
      .then(() => console.log("database droped"))
      .catch(e => {
        throw "error dropping db"
      })
    await mongoose.disconnect()
  } catch (error) {
    console.log("hubo un error", error)
    throw error
  }
}

cleanDB()
