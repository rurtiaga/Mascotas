const mongoose = require("mongoose")
const configureAndStartMongo = require("../../mongoose/configureAndStart")
const initialiseAdmin = require("./initialiseAdmin")

// Listas para generar los datos
const { male_names_list, surnames_list } = require("./nameList")
//contiene una lista de 1000 mascotas
const list_pets = require("./pets_MOCK_DATA1.json")

/**
 * Verifica que la base de datos esté vacia, si no lo está lanza error
 */
function checkEmptyDB() {
  return mongoose.connection.db
    .listCollections({}, { nameOnly: true })
    .next(function(err, collinfo) {
      if (collinfo) {
        throw "\n**************\nThis DB it is not empty, please clean the BD before try to initialise\nWe recommend try command 'dDB' for db clean\n**************\n"
      }
    })
}
/**
 * Inicializa una cantidad de usuarios recibida de usuarios
 * @param {integer} numOfUsers
 */
function initialiseUsers(numOfUsers) {
  const Users = mongoose.model("Users")
  const promises = []
  for (let indexUser = 0; indexUser < numOfUsers; indexUser++) {
    promises.push(
      Users.newUser({
        name: male_names_list[indexUser],
        last_name: surnames_list[indexUser],
        email: `${male_names_list[indexUser]}${surnames_list[indexUser]}_mascotas@gmail.com`,
        password: "1234"
      })
        .then(r => console.log("user created", r))
        .catch(e => console.log(e))
    )
  }
  return Promise.all(promises)
}

function initialisePets(cantUsers, cantPets) {
  const Users = mongoose.model("Users")
  const promises = []
  let indexActualPet = 0
  for (let iUser = 0; iUser < cantUsers; iUser++) {
    for (let iPet = 0; iPet < cantPets; iPet++) {
      promises.push(
        Users.findOne({ name: male_names_list[iUser] }).then(u => {
          pet = list_pets[indexActualPet++]
          pet._id = pet.id.$oid
          u.addPet(pet)
          return u.save()
        })
      )
    }
  }
  return Promise.all(promises)
}

/**
 * inicializa searchs en todas las mascotas
 * @param {int} cantUsers
 * @param {int} cantPets
 */
function initialiseSearchs(cantUsers, cantPets) {
  const Users = mongoose.model("Users")
  const cantTotalPets = cantUsers * cantPets
  //Creo un location point localizado en general belgrano
  const loc = {
    type: "Point",
    coordinates: [-35.769577, -58.498871]
  }
  const promises = []
  for (let iPet = 0; iPet < cantTotalPets; iPet++) {
    promises.push(
      Users.newSearch(list_pets[iPet].id.$oid, loc, new Date("6-6-2019"))
        .then(search_id => {
          console.log("Search created:", search_id)
        })
        .catch(e => console.log(e))
    )
  }
  return Promise.all(promises)
}

async function initialise() {
  //Cantidad de usuarios a crear
  const cantUsers = 2
  //Cantidad de mascotas por usuario
  const cantPets = 2

  await configureAndStartMongo()
  try {
    //Verifico si la bd tiene datos, si los tiene lanza un error y termina la ejecución
    await checkEmptyDB()
    await initialiseAdmin()
    await initialiseUsers(cantUsers)
    await initialisePets(cantUsers, cantPets)
    await initialiseSearchs(cantUsers, cantPets)
    await mongoose.disconnect()
  } catch (error) {
    console.log(error)
  }
}

initialise()
