const mongoose = require("mongoose")
module.exports = function initialiseAdmin() {
  const Users = mongoose.model("Users")
  return Users.newAdmin({
    name: "admin",
    last_name: "admin",
    email: `admin@admin.com`,
    password: "admin"
  })
    .then(r => console.log("ADMIN created, user:admin@admin.com pass:admin", r))
    .catch(e => console.log(e))
}
